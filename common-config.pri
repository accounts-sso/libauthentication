PROJECT_VERSION = 0.1

PROJECT_NAME = "libauthentication"

CONFIG += \
    c++11 \
    exceptions_off \
    no_keywords

win32: CONFIG += rtti_off
else: QMAKE_CXXFLAGS += -fno-rtti

# Workaround for https://bugreports.qt.io/browse/QTBUG-75210
QMAKE_CXXFLAGS += -Wno-deprecated-copy

INSTALL_PREFIX=/usr

!isEmpty(PREFIX) {
    INSTALL_PREFIX=$${PREFIX}
}

LIBDIR_REL = lib

!isEmpty(LIBDIR) {
    LIBDIR_REL = $${LIBDIR}
}

INSTALL_LIBDIR = $${INSTALL_PREFIX}/$${LIBDIR_REL}

win32:CONFIG(release, debug|release): BUILDD = "/release"
else:win32:CONFIG(debug, debug|release): BUILDD = "/debug"
else: BUILDD = ""

include(coverage.pri)
