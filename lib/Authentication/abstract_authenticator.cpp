/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "abstract_authenticator_p.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QHttpMultiPart>
#include <QJSEngine>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QStandardPaths>
#include <QUrlQuery>
#include <SignOn/UiSessionData>

using namespace Authentication;

AbstractAuthenticatorPrivate::AbstractAuthenticatorPrivate(AbstractAuthenticator *q):
    m_userNam(0),
    m_nam(0),
    m_errorCode(AbstractAuthenticator::NoError),
    m_authorizationTransmission(AbstractAuthenticator::ParametersInQuery),
    q_ptr(q)
{
    qRegisterMetaType<ActionRequest>("Authentication::ActionRequest");

    m_storeFile =
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation) +
        "/libauthentication.store";
}

AbstractAuthenticatorPrivate::~AbstractAuthenticatorPrivate()
{
    if (!m_userNam) {
        delete m_nam;
    }
}

/**
 * Subclasses will reimplement this to handle the result. Chain up to the
 * parent's implementation, which could clean up the actionRequest object */
void AbstractAuthenticatorPrivate::actionCompleted(const QUrl &url)
{
    Q_UNUSED(url);
    Q_ASSERT(m_actionRequest.isValid());
}

void AbstractAuthenticatorPrivate::actionRejected()
{
    Q_ASSERT(m_actionRequest.isValid());
    /* We could delete the actionRequest object here, if we wanted to;
     * instead, we do it either when a new request comes in, or in the
     * destructor. There's no strong reason to prefer either. */
}

bool
AbstractAuthenticatorPrivate::buildRequestFull(AuthenticatedRequest &request,
                                               const QUrl &url,
                                               const QByteArray &verb,
                                               const QVariantMap &params) const
{
    bool ret = buildRequest(request, url, verb, params);

    if (!m_userAgent.isEmpty()) {
        request.headers.append({ "User-Agent", m_userAgent.toUtf8() });
    }

    return ret;
}

void AbstractAuthenticatorPrivate::requestAction(ActionRequest actionRequest)
{
    Q_Q(AbstractAuthenticator);

    m_actionRequest = actionRequest;

    Q_EMIT q->actionRequested(actionRequest);
}

QUrl AbstractAuthenticatorPrivate::composeUrl(const QUrl &url,
                                              const QVariantMap &params)
{
    QUrlQuery query;
    for (auto i = params.constBegin(); i != params.constEnd(); i++) {
        query.addQueryItem(i.key(), i.value().toString());
    }
    QUrl completeUrl(url);
    completeUrl.setQuery(query);
    return completeUrl;
}

void AbstractAuthenticatorPrivate::addPartsFromQuery(QHttpMultiPart *multiPart,
                                                     const QUrlQuery &query)
{
    QByteArray contentDisposition("form-data; name=\"");

    QList<QPair<QString, QString> > items =
        query.queryItems(QUrl::FullyDecoded);
    Q_FOREACH(const auto &pair, items) {
        QHttpPart part;
        part.setHeader(QNetworkRequest::ContentDispositionHeader,
                       contentDisposition + pair.first + '"');
        part.setBody(pair.second.toUtf8());
        multiPart->append(part);
    }
}

void AbstractAuthenticatorPrivate::addPart(QHttpMultiPart *multiPart,
                                           const QString &name,
                                           const QVariantMap &map)
{
    QHttpPart part;
    QByteArray contentDisposition = "form-data; name=\"" +
        name.toLatin1() + "\"";
    if (map.contains("fileName")) {
        contentDisposition += "; filename=\"" +
            map.value("fileName").toString().toUtf8() + "\"";
    }
    part.setHeader(QNetworkRequest::ContentDispositionHeader,
                   contentDisposition);
    if (map.contains("contentType")) {
        part.setHeader(QNetworkRequest::ContentTypeHeader,
                       map.value("contentType").toByteArray());
    }

    if (map.value("base64").toBool()) {
        part.setRawHeader("Content-Transfer-Encoding", "base64");
        QFile file(map.value("filePath").toString());
        file.open(QIODevice::ReadOnly);
        part.setBody(file.readAll().toBase64());
    } else {
        QFile *file = new QFile(map.value("filePath").toString(), multiPart);
        file->open(QIODevice::ReadOnly);
        part.setBodyDevice(file);
    }
    multiPart->append(part);
}

bool
AbstractAuthenticatorPrivate::parseParameters(AuthenticatedRequest &request,
                                              const QUrl &url,
                                              const QByteArray &verb,
                                              const QVariantMap &params)
{
    bool bodyless = (verb == "GET" || verb == "HEAD");
    QHttpMultiPart *multiPart = 0;

    QUrlQuery query(url);
    for (auto i = params.constBegin(); i != params.constEnd(); i++) {
        const QVariant &v = i.value();
        if (v.canConvert<QVariantMap>()) {
            const QVariantMap value = v.toMap();
            /* When finding a dictionary under the "headers" name, we assume
             * that this is a container for extra HTTP headers. */
            if (i.key() == "headers") {
                for (auto j = value.begin(); j != value.end(); j++) {
                    request.headers.append({
                        j.key().toUtf8(), j.value().toString().toUtf8()
                    });
                }
                continue;
            }

            /* this parameter is a file upload; we cannot just send it as
             * another parameter; instead, we must send the request as a
             * multi-part message */
            if (!multiPart) {
                if (bodyless) {
                    qWarning() << "Can't send files with" << verb;
                    return false;
                }
                multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
            }
            addPart(multiPart, i.key(), value);
            continue;
        }
        query.addQueryItem(i.key(), v.toString());
    }

    if (multiPart) {
        request.headers.append({
            "Content-Type", "multipart/form-data; boundary=\"" +
            multiPart->boundary() + "\";charset=UTF-8"
        });
    }

    request.url = url.adjusted(QUrl::RemoveQuery);
    request.url.setQuery(query);
    request.multiPart = multiPart;
    return true;
}

void AbstractAuthenticatorPrivate::handleUserActionRequired(
    const SignOn::UiSessionData &data)
{
    Q_Q(AbstractAuthenticator);
    ActionRequest request(q,
                          data.OpenUrl(),
                          data.FinalUrl());
    requestAction(request);
}

void AbstractAuthenticatorPrivate::handleStore(const SignOn::SessionData &data)
{
    storeData(data.toMap());
}

void AbstractAuthenticatorPrivate::storeData(const QVariantMap &data)
{
    QFileInfo fileInfo(m_storeFile);
    if (Q_UNLIKELY(!QDir::root().mkpath(fileInfo.path()))) {
        qWarning() << "Could not create store dir!";
        return;
    }

    QFile file(m_storeFile);
    if (Q_UNLIKELY(!file.open(QIODevice::ReadWrite))) {
        qWarning() << "Could not create store file" << m_storeFile;
        return;
    }

    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    QJsonObject json = doc.object();
    json.insert(m_accountId, QJsonObject::fromVariantMap(data));
    doc = QJsonDocument(json);

    file.resize(0);
    file.write(doc.toJson());
}

QVariantMap AbstractAuthenticatorPrivate::storedData() const
{
    QFile file(m_storeFile);
    if (!file.open(QIODevice::ReadOnly)) {
        return QVariantMap();
    }

    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    QJsonObject json = doc.object();
    return json.value(m_accountId).toObject().toVariantMap();
}

QJSValue AbstractAuthenticatorPrivate::buildXMLHttpRequest(const QUrl &url,
                                            const QByteArray &verb,
                                            const QVariantMap &params,
                                            bool async)
{
    Q_Q(AbstractAuthenticator);

    AuthenticatedRequest request;
    request.authorizationTransmission = m_authorizationTransmission;
    bool ok = buildRequestFull(request, url, verb, params);
    if (Q_UNLIKELY(!ok)) return QJSValue();

    QJSValue xmlHttpRequest = newXmlHttpRequest();

    QJSValue open = xmlHttpRequest.property("open");
    QJSValueList args {
        QJSValue(QString::fromLatin1(verb)),
        QJSValue(request.url.toString()),
        QJSValue(async),
    };
    open.callWithInstance(xmlHttpRequest, args);

    QJSValue setRequestHeader = xmlHttpRequest.property("setRequestHeader");
    for (const auto &header: request.headers) {
        args = {
            QJSValue(QString::fromLatin1(header.first)),
            QJSValue(QString::fromLatin1(header.second)),
        };
        setRequestHeader.callWithInstance(xmlHttpRequest, args);
    }

    QByteArray body = request.multiPart ?
        m_multipart.toByteArray(request.multiPart) : request.body;
    delete request.multiPart;

    /* Passing the body to XMLHttpRequest.send() will fail until QTBUG-61599 is
     * fixed, unless the body consists of utf-8 data only. A workaround for
     * uploading binary files, is to encode them with base64; but not all
     * websites support that.
     */
    xmlHttpRequest.setProperty("body", qjsEngine(q)->toScriptValue(body));
    return xmlHttpRequest;
}

QJSValue AbstractAuthenticatorPrivate::newXmlHttpRequest()
{
    Q_Q(AbstractAuthenticator);
    if (m_xmlHttpRequestConstructor.isUndefined()) {
        QJSEngine *engine = qjsEngine(q);
        m_xmlHttpRequestConstructor =
            engine->globalObject().property("XMLHttpRequest");
    }
    return m_xmlHttpRequestConstructor.callAsConstructor({});
}

AbstractAuthenticator::AbstractAuthenticator(AbstractAuthenticatorPrivate *d,
                                             QObject *parent):
    QObject(parent),
    d_ptr(d)
{
}

AbstractAuthenticator::~AbstractAuthenticator()
{
}

void AbstractAuthenticator::setUserAgent(const QString &userAgent)
{
    Q_D(AbstractAuthenticator);
    d->m_userAgent = userAgent;
    Q_EMIT userAgentChanged();
}

QString AbstractAuthenticator::userAgent() const
{
    Q_D(const AbstractAuthenticator);
    return d->m_userAgent;
}

void AbstractAuthenticator::setNetworkAccessManager(QNetworkAccessManager *nam)
{
    Q_D(AbstractAuthenticator);
    if (nam && d->m_nam != d->m_userNam) {
        delete d->m_nam;
        d->m_nam = 0;
    }
    d->m_userNam = nam;
}

QNetworkAccessManager *AbstractAuthenticator::networkAccessManager() const
{
    Q_D(const AbstractAuthenticator);
    d->ensureNam();
    return d->m_nam;
}

void AbstractAuthenticator::setError(AbstractAuthenticator::ErrorCode code,
                                     const QString &message)
{
    Q_D(AbstractAuthenticator);
    d->m_errorCode = code;
    d->m_errorMessage = message;
}

AbstractAuthenticator::ErrorCode AbstractAuthenticator::errorCode() const
{
    Q_D(const AbstractAuthenticator);
    return d->m_errorCode;
}

QString AbstractAuthenticator::errorMessage() const
{
    Q_D(const AbstractAuthenticator);
    return d->m_errorMessage;
}

void AbstractAuthenticator::setAuthorizationTransmission(ParameterTransmission transmission)
{
    Q_D(AbstractAuthenticator);
    d->m_authorizationTransmission = transmission;
    Q_EMIT authorizationTransmissionChanged();
}

AbstractAuthenticator::ParameterTransmission AbstractAuthenticator::authorizationTransmission() const
{
    Q_D(const AbstractAuthenticator);
    return d->m_authorizationTransmission;
}

void AbstractAuthenticator::setAccountId(const QString &accountId)
{
    Q_D(AbstractAuthenticator);
    if (accountId == d->m_accountId) return;
    d->m_accountId = accountId;
    Q_EMIT accountIdChanged();
}

QString AbstractAuthenticator::accountId() const
{
    Q_D(const AbstractAuthenticator);
    return d->m_accountId;
}

QJSValue AbstractAuthenticator::newGetRequest(const QUrl &url,
                                              const QVariantMap &params,
                                              bool async)
{
    Q_D(AbstractAuthenticator);
    return d->buildXMLHttpRequest(url, "GET", params, async);
}

QNetworkReply *AbstractAuthenticator::get(const QUrl &url,
                                          const QVariantMap &params)
{
    Q_D(const AbstractAuthenticator);

    AuthenticatedRequest request;
    request.authorizationTransmission = d->m_authorizationTransmission;
    bool ok = d->buildRequestFull(request, url, "GET", params);
    if (Q_UNLIKELY(!ok)) return 0;

    QNetworkRequest req(request.url);
    for (const auto &header: request.headers) {
        req.setRawHeader(header.first, header.second);
    }
    return networkAccessManager()->get(req);
}

QJSValue AbstractAuthenticator::newPostRequest(const QUrl &url,
                                               const QVariantMap &params,
                                               bool async)
{
    Q_D(AbstractAuthenticator);
    return d->buildXMLHttpRequest(url, "POST", params, async);
}

QNetworkReply *AbstractAuthenticator::post(const QUrl &url,
                                           const QVariantMap &params)
{
    Q_D(AbstractAuthenticator);

    AuthenticatedRequest request;
    request.authorizationTransmission = d->m_authorizationTransmission;
    bool ok = d->buildRequestFull(request, url, "POST", params);
    if (Q_UNLIKELY(!ok)) return 0;

    QNetworkRequest req(request.url);
    for (const auto &header: request.headers) {
        req.setRawHeader(header.first, header.second);
    }
    QNetworkReply *reply;
    if (request.multiPart) {
        reply = networkAccessManager()->post(req, request.multiPart);
        request.multiPart->setParent(reply);
    } else {
        reply = networkAccessManager()->post(req, request.body);
    }
    return reply;
}

QNetworkRequest
AbstractAuthenticator::authorizedRequest(const QByteArray &verb,
                                         const QUrl &url,
                                         const QVariantMap &params) const
{
    Q_D(const AbstractAuthenticator);
    AuthenticatedRequest request;
    d->buildRequestFull(request, url, verb, params);
    QNetworkRequest req(request.url);
    for (const auto &header: request.headers) {
        req.setRawHeader(header.first, header.second);
    }
    return req;
}

QJSValue
AbstractAuthenticator::newRequest(const QByteArray &verb,
                                  const QUrl &url,
                                  const QVariantMap &params,
                                  bool async)
{
    Q_D(AbstractAuthenticator);
    return d->buildXMLHttpRequest(url, verb, params, async);
}

void AbstractAuthenticator::logout()
{
    Q_D(AbstractAuthenticator);

    /* clear the stored data */
    d->storeData(QVariantMap());

    /* subclasses might want to reset their internal state as well */
}
