/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_ABSTRACT_AUTHENTICATOR_P_H
#define AUTHENTICATION_ABSTRACT_AUTHENTICATOR_P_H

#include "abstract_authenticator.h"

#include <QNetworkAccessManager>
#include <QPair>
#include <QUrl>
#include <QVector>
#include "multipart.h"

namespace SignOn {
class SessionData;
class UiSessionData;
}

namespace Authentication {

struct AuthenticatedRequest {
    AbstractAuthenticator::ParameterTransmission authorizationTransmission =
        AbstractAuthenticator::ParametersInHeader;
    QUrl url;
    QVector<QPair<QByteArray,QByteArray>> headers;
    QByteArray body;
    QHttpMultiPart *multiPart = nullptr;
};

class AbstractAuthenticatorPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(AbstractAuthenticator)

public:
    AbstractAuthenticatorPrivate(AbstractAuthenticator *q);
    ~AbstractAuthenticatorPrivate();

    void ensureNam() const;

    virtual void actionCompleted(const QUrl &url);
    virtual void actionRejected();

    virtual bool buildRequest(AuthenticatedRequest &request,
                              const QUrl &url,
                              const QByteArray &verb,
                              const QVariantMap &params) const = 0;
    bool buildRequestFull(AuthenticatedRequest &request,
                          const QUrl &url,
                          const QByteArray &verb,
                          const QVariantMap &params) const;
    QJSValue buildXMLHttpRequest(const QUrl &url, const QByteArray &verb,
                                 const QVariantMap &params, bool async);

    void requestAction(ActionRequest actionRequest);
    void handleUserActionRequired(const SignOn::UiSessionData &data);

    void handleStore(const SignOn::SessionData &data);
    void storeData(const QVariantMap &data);
    QVariantMap storedData() const;

    QJSValue newXmlHttpRequest();

    static QUrl composeUrl(const QUrl &url, const QVariantMap &params);

    static bool parseParameters(AuthenticatedRequest &request,
                                const QUrl &url,
                                const QByteArray &verb,
                                const QVariantMap &params);
    static void addPartsFromQuery(QHttpMultiPart *multiPart,
                                  const QUrlQuery &query);
    static void addPart(QHttpMultiPart *multiPart, const QString &name,
                        const QVariantMap &map);

protected:
    QString m_accountId;
    QString m_storeFile;
    QString m_userAgent;
    QNetworkAccessManager *m_userNam;
    mutable QNetworkAccessManager *m_nam;
    AbstractAuthenticator::ErrorCode m_errorCode;
    QString m_errorMessage;
    AbstractAuthenticator::ParameterTransmission m_authorizationTransmission;
    ActionRequest m_actionRequest;
    QJSValue m_xmlHttpRequestConstructor;
    Multipart m_multipart;
    AbstractAuthenticator *q_ptr;
};

inline void AbstractAuthenticatorPrivate::ensureNam() const
{
    if (!m_nam) {
        m_nam = m_userNam ? m_userNam : new QNetworkAccessManager;
    }
}

} // namespace

#endif // AUTHENTICATION_ABSTRACT_AUTHENTICATOR_P_H
