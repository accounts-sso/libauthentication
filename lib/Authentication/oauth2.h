/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_OAUTH2_H
#define AUTHENTICATION_OAUTH2_H

#include <Authentication/AbstractAuthenticator>
#include <Authentication/global.h>
#include <QByteArray>
#include <QObject>
#include <QScopedPointer>
#include <QStringList>
#include <QVariantMap>

namespace Authentication {

class OAuth2Private;
class AUTH_EXPORT OAuth2: public AbstractAuthenticator
{
    Q_OBJECT
    Q_PROPERTY(QString clientId READ clientId WRITE setClientId \
               NOTIFY clientIdChanged)
    Q_PROPERTY(QString clientSecret READ clientSecret WRITE setClientSecret \
               NOTIFY clientSecretChanged)
    Q_PROPERTY(QStringList scopes READ scopes WRITE setScopes \
               NOTIFY scopesChanged)
    Q_PROPERTY(QUrl authorizationUrl READ authorizationUrl \
               WRITE setAuthorizationUrl NOTIFY authorizationUrlChanged)
    Q_PROPERTY(QUrl accessTokenUrl READ accessTokenUrl \
               WRITE setAccessTokenUrl NOTIFY accessTokenUrlChanged)
    Q_PROPERTY(QString responseType READ responseType WRITE setResponseType \
               NOTIFY responseTypeChanged)
    Q_PROPERTY(QString callbackUrl READ callbackUrl WRITE setCallbackUrl \
               NOTIFY callbackUrlChanged)
    Q_PROPERTY(ProtocolTweaks protocolTweaks READ protocolTweaks \
               WRITE setProtocolTweaks NOTIFY protocolTweaksChanged)
    Q_PROPERTY(QByteArray accessToken READ accessToken NOTIFY finished)
    Q_PROPERTY(QByteArray refreshToken READ refreshToken NOTIFY finished)
    Q_PROPERTY(int expiresIn READ expiresIn NOTIFY finished)
    Q_PROPERTY(QStringList grantedPermissions READ grantedPermissions \
               NOTIFY finished)
    Q_PROPERTY(QVariantMap extraFields READ extraFields NOTIFY finished)

public:
    enum ProtocolTweak {
        NoTweaks = 0,
        DisableStateParameter = 1 << 0,
        ClientAuthInRequestBody = 1 << 1,
    };
    Q_DECLARE_FLAGS(ProtocolTweaks, ProtocolTweak)
    Q_FLAG(ProtocolTweaks)

    OAuth2(QObject *parent = 0);
    ~OAuth2();

    void process() override;
    void logout() override;

    void setClientId(const QString &clientId);
    QString clientId() const;

    void setClientSecret(const QString &clientSecret);
    QString clientSecret() const;

    void setScopes(const QStringList &scopes);
    QStringList scopes() const;

    void setAuthorizationUrl(const QUrl &authorizationUrl);
    QUrl authorizationUrl() const;

    void setAccessTokenUrl(const QUrl &accessTokenUrl);
    QUrl accessTokenUrl() const;

    void setResponseType(const QString &responseType);
    QString responseType() const;

    void setCallbackUrl(const QString &callbackUrl);
    QString callbackUrl() const;

    void setProtocolTweaks(ProtocolTweaks flags);
    ProtocolTweaks protocolTweaks() const;

    /* Response data */
    QByteArray accessToken() const;
    QByteArray refreshToken() const;
    int expiresIn() const;
    QStringList grantedPermissions() const;
    QVariantMap extraFields() const;

    Q_INVOKABLE void injectToken(const QByteArray &accessToken);

Q_SIGNALS:
    void clientIdChanged();
    void clientSecretChanged();
    void scopesChanged();
    void authorizationUrlChanged();
    void accessTokenUrlChanged();
    void responseTypeChanged();
    void callbackUrlChanged();
    void protocolTweaksChanged();
    void finished(); // moc needs this to be here too :-/

private:
    Q_DECLARE_PRIVATE(OAuth2)
};

} // namespace

Q_DECLARE_OPERATORS_FOR_FLAGS(Authentication::OAuth2::ProtocolTweaks)

#endif // AUTHENTICATION_OAUTH2_H
