/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_ABSTRACT_AUTHENTICATOR_H
#define AUTHENTICATION_ABSTRACT_AUTHENTICATOR_H

#include <Authentication/ActionRequest>
#include <Authentication/global.h>
#include <QJSValue>
#include <QNetworkRequest>
#include <QObject>
#include <QScopedPointer>

class QNetworkAccessManager;
class QNetworkReply;

namespace Authentication {

class ActionRequest;

class AbstractAuthenticatorPrivate;
class AUTH_EXPORT AbstractAuthenticator: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userAgent READ userAgent WRITE setUserAgent \
               NOTIFY userAgentChanged)
    Q_PROPERTY(bool hasError READ hasError NOTIFY finished)
    Q_PROPERTY(QString errorMessage READ errorMessage NOTIFY finished)
    Q_PROPERTY(ErrorCode errorCode READ errorCode NOTIFY finished)
    Q_PROPERTY(ParameterTransmission authorizationTransmission \
               READ authorizationTransmission \
               WRITE setAuthorizationTransmission \
               NOTIFY authorizationTransmissionChanged)
    Q_PROPERTY(QString accountId READ accountId NOTIFY accountIdChanged)

public:
    enum ErrorCode {
        NoError = 0,
        InternalError,
        ServerError,
        MissingData,
        NotAuthorized,
        UserCanceled,
    };
    Q_ENUM(ErrorCode)

    enum ParameterTransmission {
        ParametersInQuery = 0,
        ParametersInHeader,
        ParametersInBody,
    };
    Q_ENUM(ParameterTransmission)

    ~AbstractAuthenticator();

    void setUserAgent(const QString &userAgent);
    QString userAgent() const;

    void setNetworkAccessManager(QNetworkAccessManager *nam);
    QNetworkAccessManager *networkAccessManager() const;

    bool hasError() const { return errorCode() != NoError; }
    ErrorCode errorCode() const;
    QString errorMessage() const;

    void setAuthorizationTransmission(ParameterTransmission transmission);
    ParameterTransmission authorizationTransmission() const;

    QString accountId() const;

    Q_INVOKABLE virtual void process() = 0;
    Q_INVOKABLE virtual QJSValue newGetRequest(const QUrl &url,
                                               const QVariantMap &params,
                                               bool async = true);
    virtual QNetworkReply *get(const QUrl &url, const QVariantMap &params);

    Q_INVOKABLE virtual QJSValue newPostRequest(const QUrl &url,
                                                const QVariantMap &params,
                                                bool async = true);
    virtual QNetworkReply *post(const QUrl &url,
                                const QVariantMap &params);

    virtual QNetworkRequest
        authorizedRequest(const QByteArray &verb,
                          const QUrl &url,
                          const QVariantMap &params = QVariantMap()) const;
    Q_INVOKABLE QJSValue newRequest(const QByteArray &verb,
                                    const QUrl &url,
                                    const QVariantMap &params,
                                    bool async = true);

    Q_INVOKABLE virtual void logout();

Q_SIGNALS:
    void userAgentChanged();
    void authorizationTransmissionChanged();
    void accountIdChanged();
    void actionRequested(Authentication::ActionRequest request);
    void finished();

protected:
    AbstractAuthenticator(AbstractAuthenticatorPrivate *d, QObject *parent);
    void setError(ErrorCode code, const QString &message);
    void setAccountId(const QString &accountId);

    friend class ActionRequest;
    QScopedPointer<AbstractAuthenticatorPrivate> d_ptr;
    Q_DECLARE_PRIVATE(AbstractAuthenticator)
};

} // namespace

#endif // AUTHENTICATION_ABSTRACT_AUTHENTICATOR_H
