/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "oauth2.h"

#include <QDebug>
#include <QHttpMultiPart>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <SignOn/AuthPluginInterface>
#include "abstract_authenticator_p.h"
#include "plugin_loader.h"

// From SignOn plugin:
#include <oauth2data.h>

using namespace Authentication;

namespace Authentication {

class OAuth2Private: public AbstractAuthenticatorPrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(OAuth2)

public:
    OAuth2Private(OAuth2 *q);
    ~OAuth2Private();

    bool ensurePlugin();
    void process();

    QString mechanism() const;

    void actionCompleted(const QUrl &url) override;
    void actionRejected() override;

    bool buildRequest(AuthenticatedRequest &request,
                      const QUrl &url,
                      const QByteArray &verb,
                      const QVariantMap &params) const override;

private Q_SLOTS:
    void onResult(const SignOn::SessionData &data);
    void onUserActionRequired(const SignOn::UiSessionData &data);
    void onError(const SignOn::Error &err);

private:
    AuthPluginInterface *m_plugin;
    OAuth2PluginNS::OAuth2PluginData m_sessionData;
    OAuth2PluginNS::OAuth2PluginTokenData m_reply;
};

} // namespace

OAuth2Private::OAuth2Private(OAuth2 *q):
    AbstractAuthenticatorPrivate(q),
    m_plugin(0)
{
    m_authorizationTransmission = AbstractAuthenticator::ParametersInHeader;
}

OAuth2Private::~OAuth2Private()
{
    delete m_plugin;
}

bool OAuth2Private::ensurePlugin()
{
    Q_Q(OAuth2);

    if (m_plugin) return true;

    m_plugin = PluginLoader::load("oauth2");
    if (Q_UNLIKELY(!m_plugin)) {
        q->setError(AbstractAuthenticator::InternalError,
                    QStringLiteral("Couldn't load OAuth plugin"));
        Q_EMIT q->finished();
        return false;
    }

    QObject::connect(m_plugin, &AuthPluginInterface::result,
                     this, &OAuth2Private::onResult);
    QObject::connect(m_plugin, &AuthPluginInterface::userActionRequired,
                     this, &OAuth2Private::onUserActionRequired);
    QObject::connect(m_plugin, &AuthPluginInterface::store,
                     this, &AbstractAuthenticatorPrivate::handleStore);
    QObject::connect(m_plugin, &AuthPluginInterface::error,
                     this, &OAuth2Private::onError);

    return true;
}

void OAuth2Private::process()
{
    if (!ensurePlugin()) return;

    SignOn::SessionData sessionData(m_sessionData);
    SignOn::SessionData data(storedData());
    sessionData += data;
    m_plugin->process(sessionData, mechanism());
}

QString OAuth2Private::mechanism() const
{
    QStringList responseType = m_sessionData.ResponseType();
    if (responseType.contains("token")) {
        return QStringLiteral("user_agent");
    }

    if (responseType.contains("code")) {
        return QStringLiteral("web_server");
    }

    return QString();
}

void OAuth2Private::actionCompleted(const QUrl &url)
{
    SignOn::UiSessionData data;
    data.setUrlResponse(url.toString());
    m_plugin->userActionFinished(data);

    AbstractAuthenticatorPrivate::actionCompleted(url);
}

void OAuth2Private::actionRejected()
{
    SignOn::UiSessionData data;
    data.setQueryErrorCode(SignOn::QUERY_ERROR_CANCELED);
    m_plugin->userActionFinished(data);

    AbstractAuthenticatorPrivate::actionRejected();
}

bool OAuth2Private::buildRequest(AuthenticatedRequest &request,
                                 const QUrl &url,
                                 const QByteArray &verb,
                                 const QVariantMap &params) const
{
    Q_Q(const OAuth2);

    bool bodyless = (verb == "GET" || verb == "HEAD");

    if (!parseParameters(request, url, verb, params)) return false;

    QUrlQuery query(request.url);
    if (m_authorizationTransmission == AbstractAuthenticator::ParametersInHeader) {
        request.headers.append(
            { "Authorization", QByteArray("Bearer ") + q->accessToken() }
            );
    } else {
        query.addQueryItem("access_token", q->accessToken());
    }

    if (m_authorizationTransmission == AbstractAuthenticator::ParametersInQuery ||
        bodyless) {
        request.url.setQuery(query);
    } else {
        request.url = url.adjusted(QUrl::RemoveQuery);
        if (request.multiPart) {
            addPartsFromQuery(request.multiPart, query);
        } else {
            request.body = query.toString().toUtf8();
        }
    }

    if (!request.multiPart && !request.body.isEmpty()) {
        request.headers.append({
            "Content-Type", QByteArrayLiteral("application/x-www-form-urlencoded")
        });
    }

    return true;
}

void OAuth2Private::onResult(const SignOn::SessionData &data)
{
    Q_Q(OAuth2);

    m_reply = static_cast<const OAuth2PluginNS::OAuth2PluginTokenData&>(data);
    q->finished();
}

void OAuth2Private::onUserActionRequired(const SignOn::UiSessionData &data)
{
    handleUserActionRequired(data);
}

void OAuth2Private::onError(const SignOn::Error &err)
{
    Q_Q(OAuth2);

    AbstractAuthenticator::ErrorCode code = AbstractAuthenticator::InternalError;

    switch (err.type()) {
    case SignOn::Error::MissingData:
        code = AbstractAuthenticator::MissingData;
        break;
    case SignOn::Error::NotAuthorized:
    case SignOn::Error::InvalidCredentials:
        code = AbstractAuthenticator::NotAuthorized;
        break;
    }

    q->setError(code, err.message());
    Q_EMIT q->finished();
}

OAuth2::OAuth2(QObject *parent):
    AbstractAuthenticator(new OAuth2Private(this), parent)
{
    /* Workaround a moc limitation: when defining QObject properties, moc will
     * complain if the notify signal is not found in the very same class which
     * defines the property. So, we need to relay the finished signal on this
     * class too.
     */
    QObject::connect(this, &AbstractAuthenticator::finished,
                     this, &OAuth2::finished);
}

OAuth2::~OAuth2()
{
}

void OAuth2::process()
{
    Q_D(OAuth2);
    setError(NoError, QString());
    d->process();
}

void OAuth2::logout()
{
    Q_D(OAuth2);
    d->m_reply = OAuth2PluginNS::OAuth2PluginTokenData();
    AbstractAuthenticator::logout();
}

void OAuth2::setClientId(const QString &clientId)
{
    Q_D(OAuth2);
    d->m_sessionData.setClientId(clientId);
    Q_EMIT clientIdChanged();
}

QString OAuth2::clientId() const
{
    Q_D(const OAuth2);
    return d->m_sessionData.ClientId();
}

void OAuth2::setClientSecret(const QString &clientSecret)
{
    Q_D(OAuth2);
    d->m_sessionData.setClientSecret(clientSecret);
    Q_EMIT clientSecretChanged();
}

QString OAuth2::clientSecret() const
{
    Q_D(const OAuth2);
    return d->m_sessionData.ClientSecret();
}

void OAuth2::setScopes(const QStringList &scopes)
{
    Q_D(OAuth2);
    d->m_sessionData.setScope(scopes);
    Q_EMIT scopesChanged();
}

QStringList OAuth2::scopes() const
{
    Q_D(const OAuth2);
    return d->m_sessionData.Scope();
}

void OAuth2::setAuthorizationUrl(const QUrl &authorizationUrl)
{
    Q_D(OAuth2);
    QString host(authorizationUrl.toString(QUrl::RemovePath |
                                           QUrl::RemoveQuery |
                                           QUrl::RemoveScheme |
                                           QUrl::StripTrailingSlash));
    if (host.startsWith("//")) {
        host = host.mid(2);
    }
    d->m_sessionData.setHost(host);
    QString path = authorizationUrl.path();
    if (path.startsWith('/')) {
        path = path.mid(1);
    }
    if (authorizationUrl.hasQuery()) {
        path += "?" + authorizationUrl.query();
    }
    d->m_sessionData.setAuthPath(path);

    /* Additionally, use the domain as account ID; TODO: let the user
     * specify the account ID */
    setAccountId(authorizationUrl.host());

    Q_EMIT authorizationUrlChanged();
}

QUrl OAuth2::authorizationUrl() const
{
    Q_D(const OAuth2);
    QUrl url(QStringLiteral("https://") + d->m_sessionData.Host());
    QString path = d->m_sessionData.AuthPath();
    int questionMark = path.indexOf('?');
    if (questionMark >= 0) {
        QUrlQuery query(path.mid(questionMark + 1));
        url.setQuery(query);
        path.truncate(questionMark);
    }
    url.setPath('/' + path);
    return url;
}

void OAuth2::setAccessTokenUrl(const QUrl &accessTokenUrl)
{
    Q_D(OAuth2);
    d->m_sessionData.setTokenHost(accessTokenUrl.host());
    if (accessTokenUrl.port() > 0) {
        d->m_sessionData.setTokenPort(accessTokenUrl.port());
    }
    d->m_sessionData.setTokenPath(accessTokenUrl.path());
    if (accessTokenUrl.hasQuery()) {
        d->m_sessionData.setTokenQuery(accessTokenUrl.query());
    }
    Q_EMIT accessTokenUrlChanged();
}

QUrl OAuth2::accessTokenUrl() const
{
    Q_D(const OAuth2);
    QUrl url;
    url.setScheme("https");
    url.setHost(d->m_sessionData.TokenHost());
    quint16 port = d->m_sessionData.TokenPort();
    if (port > 0) url.setPort(port);
    url.setPath(d->m_sessionData.TokenPath());
    url.setQuery(d->m_sessionData.TokenQuery());
    return url;
}

void OAuth2::setResponseType(const QString &responseType)
{
    Q_D(OAuth2);
    d->m_sessionData.setResponseType(responseType.split(' '));
    Q_EMIT responseTypeChanged();
}

QString OAuth2::responseType() const
{
    Q_D(const OAuth2);
    return d->m_sessionData.ResponseType().join(' ');
}

void OAuth2::setCallbackUrl(const QString &callbackUrl)
{
    Q_D(OAuth2);
    d->m_sessionData.setRedirectUri(callbackUrl);
    Q_EMIT callbackUrlChanged();
}

QString OAuth2::callbackUrl() const
{
    Q_D(const OAuth2);
    return d->m_sessionData.RedirectUri();
}

void OAuth2::setProtocolTweaks(ProtocolTweaks flags)
{
    Q_D(OAuth2);
    d->m_sessionData.setDisableStateParameter(
        flags & ProtocolTweak::DisableStateParameter);
    d->m_sessionData.setForceClientAuthViaRequestBody(
        flags & ProtocolTweak::ClientAuthInRequestBody);
    Q_EMIT protocolTweaksChanged();
}

OAuth2::ProtocolTweaks OAuth2::protocolTweaks() const
{
    Q_D(const OAuth2);
    ProtocolTweaks flags;
    if (d->m_sessionData.DisableStateParameter()) {
        flags |= ProtocolTweak::DisableStateParameter;
    }
    if (d->m_sessionData.ForceClientAuthViaRequestBody()) {
        flags |= ProtocolTweak::ClientAuthInRequestBody;
    }
    return flags;
}

QByteArray OAuth2::accessToken() const
{
    Q_D(const OAuth2);
    return d->m_reply.AccessToken().toUtf8();
}

QByteArray OAuth2::refreshToken() const
{
    Q_D(const OAuth2);
    return d->m_reply.RefreshToken().toUtf8();
}

int OAuth2::expiresIn() const
{
    Q_D(const OAuth2);
    return d->m_reply.ExpiresIn();
}

QStringList OAuth2::grantedPermissions() const
{
    Q_D(const OAuth2);
    return d->m_reply.Scope();
}

QVariantMap OAuth2::extraFields() const
{
    Q_D(const OAuth2);
    return d->m_reply.ExtraFields();
}

void OAuth2::injectToken(const QByteArray &accessToken)
{
    Q_D(OAuth2);
    d->m_reply.setAccessToken(accessToken);
    Q_EMIT finished(); // notification of property change
}

#include "oauth2.moc"
