/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "plugin_loader.h"

#include <SignOn/PluginLoader>

using namespace Authentication;

namespace Authentication {

class PluginLoaderPrivate
{
public:
    PluginLoaderPrivate();
    ~PluginLoaderPrivate() = delete;

    AuthPluginInterface *load(const QString &baseName);

private:
    friend class PluginLoader;
    static PluginLoader *m_instance;
    SignOn::PluginLoader m_loader;
};

PluginLoader *PluginLoaderPrivate::m_instance = 0;

} // namespace

PluginLoaderPrivate::PluginLoaderPrivate()
{
}

AuthPluginInterface *PluginLoaderPrivate::load(const QString &baseName)
{
    return m_loader.load(baseName);
}

PluginLoader::PluginLoader():
    d_ptr(new PluginLoaderPrivate())
{
}

PluginLoader *PluginLoader::instance()
{
    if (!PluginLoaderPrivate::m_instance) {
        PluginLoaderPrivate::m_instance = new PluginLoader;
    }
    return PluginLoaderPrivate::m_instance;
}

AuthPluginInterface *PluginLoader::load(const QString &baseName)
{
    return instance()->d_ptr->load(baseName);
}
