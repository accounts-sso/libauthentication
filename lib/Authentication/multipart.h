/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_MULTIPART_H
#define AUTHENTICATION_MULTIPART_H

#include <QByteArray>
#include <QIODevice>
#include <QScopedPointer>

class QHttpMultiPart;

namespace Authentication {

class MultipartPrivate;

class Multipart
{
public:
    Multipart();
    virtual ~Multipart();

    QByteArray toByteArray(QHttpMultiPart *multiPart);

private:
    QScopedPointer<MultipartPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Multipart)
};

} // namespace

#endif // AUTHENTICATION_MULTIPART_H
