TEMPLATE = lib
TARGET = Authentication

include(../../common-config.pri)

QT += \
    network \
    qml

QMAKE_CXXFLAGS += \
    -fvisibility=hidden

# Error on undefined symbols
QMAKE_LFLAGS += $$QMAKE_LFLAGS_NOUNDEF

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib/SignOn \
    $${TOP_SRC_DIR}/plugins/signon-oauth2/src \
    ..

LIBS += -lsignon-plugins
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/SignOn$${BUILDD}

public_headers += \
    abstract_authenticator.h AbstractAuthenticator \
    action_request.h ActionRequest \
    global.h \
    http_server.h HttpServer \
    loopback_server.h LoopbackServer \
    oauth1.h OAuth1 \
    oauth2.h OAuth2

private_headers += \
    abstract_authenticator_p.h \
    multipart.h \
    plugin_loader.h

SOURCES += \
    abstract_authenticator.cpp \
    action_request.cpp \
    http_server.cpp \
    loopback_server.cpp \
    multipart.cpp \
    oauth1.cpp \
    oauth2.cpp \
    plugin_loader.cpp

HEADERS += $$public_headers $$private_headers

DEFINES += \
    BUILDING_AUTHENTICATION

headers.files = $$public_headers
headers.path = $$INSTALL_PREFIX/include/$$TARGET
INSTALLS += headers

pkgconfig.files = $${TARGET}.pc
include($${TOP_SRC_DIR}/common-pkgconfig.pri)
INSTALLS += pkgconfig

target.path = $${INSTALL_LIBDIR}
INSTALLS += target
