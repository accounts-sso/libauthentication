/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_ACTION_REQUEST_H
#define AUTHENTICATION_ACTION_REQUEST_H

#include <Authentication/global.h>
#include <QObject>
#include <QSharedPointer>
#include <QUrl>

namespace Authentication {

class AbstractAuthenticator;
class AbstractAuthenticatorPrivate;

class ActionRequestPrivate;
class AUTH_EXPORT ActionRequest final
{
    Q_GADGET
    Q_PROPERTY(QUrl url READ url CONSTANT)
    Q_PROPERTY(QUrl finalUrl READ finalUrl CONSTANT)

public:
    ActionRequest();

    bool isValid() const;
    QUrl url() const;
    QUrl finalUrl() const;

    Q_INVOKABLE void setResult(const QUrl &resultUrl);
    Q_INVOKABLE void reject();

private:
    ActionRequest(AbstractAuthenticator *authenticator);
    ActionRequest(AbstractAuthenticator *authenticator,
                  const QUrl &url, const QUrl &finalUrl);


private:
    friend class AbstractAuthenticatorPrivate;
    QSharedPointer<ActionRequestPrivate> d_ptr;
    Q_DECLARE_PRIVATE(ActionRequest)
};

} // namespace

Q_DECLARE_METATYPE(Authentication::ActionRequest)

#endif // AUTHENTICATION_ACTION_REQUEST_H
