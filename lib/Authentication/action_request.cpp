/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "action_request.h"

#include <QPointer>
#include "abstract_authenticator_p.h"

using namespace Authentication;

namespace Authentication {

class ActionRequestPrivate {
public:
    ActionRequestPrivate(AbstractAuthenticator *authenticator = 0,
                         const QUrl &url = QUrl(), const QUrl &finalUrl = QUrl());

private:
    friend class ActionRequest;
    QUrl m_url;
    QUrl m_finalUrl;
    QPointer<AbstractAuthenticator> m_authenticator;
};

} // namespace

ActionRequestPrivate::ActionRequestPrivate(AbstractAuthenticator *authenticator,
                                           const QUrl &url, const QUrl &finalUrl):
    m_url(url),
    m_finalUrl(finalUrl),
    m_authenticator(authenticator)
{
}

ActionRequest::ActionRequest():
    d_ptr(new ActionRequestPrivate())
{
}

ActionRequest::ActionRequest(AbstractAuthenticator *authenticator):
    d_ptr(new ActionRequestPrivate(authenticator))
{
}

ActionRequest::ActionRequest(AbstractAuthenticator *authenticator,
                             const QUrl &url, const QUrl &finalUrl):
    d_ptr(new ActionRequestPrivate(authenticator, url, finalUrl))
{
}

bool ActionRequest::isValid() const
{
    Q_D(const ActionRequest);
    return !d->m_authenticator.isNull();
}

QUrl ActionRequest::url() const
{
    Q_D(const ActionRequest);
    return d->m_url;
}

QUrl ActionRequest::finalUrl() const
{
    Q_D(const ActionRequest);
    return d->m_finalUrl;
}

void ActionRequest::setResult(const QUrl &url)
{
    Q_D(ActionRequest);
    if (Q_UNLIKELY(d->m_authenticator.isNull())) return;

    d->m_authenticator->d_ptr->actionCompleted(url);
}

void ActionRequest::reject()
{
    Q_D(ActionRequest);
    if (Q_UNLIKELY(d->m_authenticator.isNull())) return;

    d->m_authenticator->d_ptr->actionRejected();
}
