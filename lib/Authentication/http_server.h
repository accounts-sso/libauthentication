/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_HTTP_SERVER_H
#define AUTHENTICATION_HTTP_SERVER_H

#include "Authentication/global.h"
#include <QObject>
#include <QScopedPointer>
#include <QUrl>

namespace Authentication {

class HttpServerPrivate;
class AUTH_EXPORT HttpServer: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl callbackUrl READ callbackUrl NOTIFY callbackUrlChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)

public:
    HttpServer(QObject *parent = 0);
    ~HttpServer();

    void setPort(int port);
    int port() const;

    QUrl callbackUrl() const;

Q_SIGNALS:
    void portChanged();
    void callbackUrlChanged();
    void visited(const QUrl &url);

private:
    Q_DECLARE_PRIVATE(HttpServer)
    QScopedPointer<HttpServerPrivate> d_ptr;
};

} // namespace

#endif // AUTHENTICATION_HTTP_SERVER_H
