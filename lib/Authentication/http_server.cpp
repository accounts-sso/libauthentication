/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "http_server.h"

#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QTcpServer>
#include <QTcpSocket>

using namespace Authentication;

namespace Authentication {

class HttpServerPrivate: public QTcpServer
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(HttpServer)

public:
    HttpServerPrivate(HttpServer *q);
    ~HttpServerPrivate();

    void queueUpdate();
    void respond(QTcpSocket *socket);

private Q_SLOTS:
    void update();
    void onNewConnection();
    void onReadyRead();

private:
    bool m_updateQueued;
    int m_port;
    QByteArray m_data;
    QString m_message;
    HttpServer *q_ptr;
};

} // namespace

HttpServerPrivate::HttpServerPrivate(HttpServer *q):
    m_updateQueued(false),
    m_port(0),
    m_message(QObject::tr("Authentication completed; you can close this page.")),
    q_ptr(q)
{
    QObject::connect(this, &QTcpServer::newConnection,
                     this, &HttpServerPrivate::onNewConnection);
    queueUpdate();
}

HttpServerPrivate::~HttpServerPrivate()
{
}

void HttpServerPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

void HttpServerPrivate::update()
{
    Q_Q(HttpServer);

    m_updateQueued = false;

    if (Q_UNLIKELY(!listen(QHostAddress::Any, m_port))) {
        qWarning() << "listen failed";
        return;
    }

    Q_EMIT q->callbackUrlChanged();
}

void HttpServerPrivate::respond(QTcpSocket *socket)
{
    QByteArray html = QByteArrayLiteral("<html><head><title>") +
        qApp->applicationName().toUtf8() +
        QByteArrayLiteral("</title></head><body>") +
        m_message.toUtf8() +
        QByteArrayLiteral("</body></html>");

    QByteArray htmlSize = QString::number(html.size()).toUtf8();
    QByteArray replyMessage =
        QByteArrayLiteral("HTTP/1.0 200 OK \r\n"
                          "Content-Type: text/html; charset=\"utf-8\"\r\n"
                          "Content-Length: ") + htmlSize +
        QByteArrayLiteral("\r\n\r\n") +
        html;

    socket->write(replyMessage);
}

void HttpServerPrivate::onNewConnection()
{
    QTcpSocket *socket = nextPendingConnection();
    Q_ASSERT(socket);
    QObject::connect(socket, &QTcpSocket::disconnected,
                     socket, &QTcpSocket::deleteLater);
    QObject::connect(socket, &QTcpSocket::readyRead,
                     this, &HttpServerPrivate::onReadyRead);
}

void HttpServerPrivate::onReadyRead()
{
    Q_Q(HttpServer);

    QTcpSocket *socket = qobject_cast<QTcpSocket*>(sender());
    Q_ASSERT(socket);

    m_data += socket->readAll();
    int lineLength = m_data.indexOf('\r');
    if (lineLength < 0) {
        /* Not all data has been received; will get more later */
        return;
    }
    QByteArray firstLine = m_data.left(lineLength);
    QList<QByteArray> parts = firstLine.split(' ');
    if (parts.count() >= 2) {
        QString path = QString::fromUtf8(parts[1]);
        QUrl url(QString::fromLatin1("http://localhost:%1%2").
                 arg(serverPort()).arg(path));
        Q_EMIT q->visited(url);
    }

    respond(socket);
    socket->disconnectFromHost();
}

HttpServer::HttpServer(QObject *parent):
    QObject(parent),
    d_ptr(new HttpServerPrivate(this))
{
}

HttpServer::~HttpServer()
{
}

void HttpServer::setPort(int port)
{
    Q_D(HttpServer);
    if (port == d->m_port) return;
    d->m_port = port;
    Q_EMIT portChanged();
}

int HttpServer::port() const
{
    Q_D(const HttpServer);
    return d->m_port;
}

QUrl HttpServer::callbackUrl() const
{
    Q_D(const HttpServer);
    if (!d->isListening())
        return QUrl();
    return QUrl(QString::fromLatin1("http://localhost:%1/").
                arg(d->serverPort()));
}

#include "http_server.moc"
