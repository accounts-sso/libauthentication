/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_PLUGIN_LOADER_H
#define AUTHENTICATION_PLUGIN_LOADER_H

#include <Authentication/global.h>

class AuthPluginInterface;

namespace Authentication {

class PluginLoaderPrivate;
class PluginLoader
{
public:
    static PluginLoader *instance();
    ~PluginLoader() = delete;

    static AuthPluginInterface *load(const QString &baseName);

protected:
    PluginLoader();

    PluginLoaderPrivate *d_ptr;
    Q_DECLARE_PRIVATE(PluginLoader)
};

} // namespace

#endif // AUTHENTICATION_PLUGIN_LOADER_H
