/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "qml_plugin.h"

#include "qml_loopback_server.h"

#include <Authentication/HttpServer>
#include <Authentication/OAuth1>
#include <Authentication/OAuth2>
#include <QDebug>
#include <QQmlComponent>

using namespace Authentication;

void Plugin::registerTypes(const char *uri)
{
    qmlRegisterType<HttpServer>(uri, 0, 1, "HttpServer");
    qmlRegisterType<OAuth1>(uri, 0, 1, "OAuth1");
    qmlRegisterType<OAuth2>(uri, 0, 1, "OAuth2");

    qmlRegisterType<QmlLoopbackServer>(uri, 0, 2, "LoopbackServer");
}
