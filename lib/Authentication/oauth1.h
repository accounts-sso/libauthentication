/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_OAUTH1_H
#define AUTHENTICATION_OAUTH1_H

#include <Authentication/AbstractAuthenticator>
#include <Authentication/global.h>
#include <QByteArray>
#include <QObject>
#include <QScopedPointer>
#include <QStringList>

namespace Authentication {

class OAuth1Private;
class AUTH_EXPORT OAuth1: public AbstractAuthenticator
{
    Q_OBJECT
    Q_PROPERTY(QString clientId READ clientId WRITE setClientId \
               NOTIFY clientIdChanged)
    Q_PROPERTY(QString clientSecret READ clientSecret WRITE setClientSecret \
               NOTIFY clientSecretChanged)
    Q_PROPERTY(QUrl temporaryCredentialsUrl READ temporaryCredentialsUrl \
               WRITE setTemporaryCredentialsUrl \
               NOTIFY temporaryCredentialsUrlChanged)
    Q_PROPERTY(QUrl authorizationUrl READ authorizationUrl \
               WRITE setAuthorizationUrl NOTIFY authorizationUrlChanged)
    Q_PROPERTY(QUrl tokenRequestUrl READ tokenRequestUrl \
               WRITE setTokenRequestUrl NOTIFY tokenRequestUrlChanged)
    Q_PROPERTY(QString signatureMethod READ signatureMethod \
               WRITE setSignatureMethod NOTIFY signatureMethodChanged)
    Q_PROPERTY(QString realm READ realm WRITE setRealm NOTIFY realmChanged)
    Q_PROPERTY(QString callbackUrl READ callbackUrl WRITE setCallbackUrl \
               NOTIFY callbackUrlChanged)
    Q_PROPERTY(QByteArray token READ token NOTIFY finished)
    Q_PROPERTY(QByteArray tokenSecret READ tokenSecret NOTIFY finished)

public:
    OAuth1(QObject *parent = 0);
    ~OAuth1();

    void process() override;
    void logout() override;

    void setClientId(const QString &clientId);
    QString clientId() const;

    void setClientSecret(const QString &clientSecret);
    QString clientSecret() const;

    void setTemporaryCredentialsUrl(const QUrl &temporaryCredentialsUrl);
    QUrl temporaryCredentialsUrl() const;

    void setAuthorizationUrl(const QUrl &authorizationUrl);
    QUrl authorizationUrl() const;

    void setTokenRequestUrl(const QUrl &tokenRequestUrl);
    QUrl tokenRequestUrl() const;

    void setSignatureMethod(const QString &signatureMethod);
    QString signatureMethod() const;

    void setRealm(const QString &realm);
    QString realm() const;

    void setCallbackUrl(const QString &callbackUrl);
    QString callbackUrl() const;

    /* Response data */
    QByteArray token() const;
    QByteArray tokenSecret() const;

    Q_INVOKABLE void injectTokens(const QByteArray &token,
                                  const QByteArray &tokenSecret);

Q_SIGNALS:
    void clientIdChanged();
    void clientSecretChanged();
    void temporaryCredentialsUrlChanged();
    void authorizationUrlChanged();
    void tokenRequestUrlChanged();
    void signatureMethodChanged();
    void realmChanged();
    void callbackUrlChanged();
    void finished(); // moc needs this to be here too :-/

private:
    Q_DECLARE_PRIVATE(OAuth1)
};

} // namespace

#endif // AUTHENTICATION_OAUTH1_H
