/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "oauth1.h"

#include <QCryptographicHash>
#include <QDebug>
#include <QHttpMultiPart>
#include <QProcessEnvironment>
#include <QUrl>
#include <QUrlQuery>
#include <SignOn/AuthPluginInterface>
#include "abstract_authenticator_p.h"
#include "plugin_loader.h"

// From SignOn plugin:
#include <oauth1data.h>

using namespace Authentication;

namespace Authentication {

class OAuth1Private: public AbstractAuthenticatorPrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(OAuth1)

public:
    OAuth1Private(OAuth1 *q);
    ~OAuth1Private();

    bool ensurePlugin();
    void process();

    void actionCompleted(const QUrl &url) override;
    void actionRejected() override;

    bool buildRequest(AuthenticatedRequest &request,
                      const QUrl &url,
                      const QByteArray &verb,
                      const QVariantMap &params) const override;

    QUrlQuery signedQuery(const QUrl &url,
                          const QByteArray &verb,
                          const QUrlQuery &params) const;
    QByteArray authorizationHeader(const QUrlQuery &signedQuery,
                                   QUrlQuery &params) const;
    QByteArray signRequest(const QUrl &requestUrl, const QByteArray &verb,
                           const QUrlQuery &query) const;
    QByteArray hashHMACSHA1(const QByteArray &baseString,
                            const QByteArray &secret) const;

private Q_SLOTS:
    void onResult(const SignOn::SessionData &data);
    void onUserActionRequired(const SignOn::UiSessionData &data);
    void onError(const SignOn::Error &err);

private:
    AuthPluginInterface *m_plugin;
    QString m_mechanism;
    OAuth2PluginNS::OAuth1PluginData m_sessionData;
    OAuth2PluginNS::OAuth1PluginTokenData m_reply;
};

} // namespace

OAuth1Private::OAuth1Private(OAuth1 *q):
    AbstractAuthenticatorPrivate(q),
    m_plugin(0)
{
}

OAuth1Private::~OAuth1Private()
{
    delete m_plugin;
}

bool OAuth1Private::ensurePlugin()
{
    Q_Q(OAuth1);

    if (m_plugin) return true;

    m_plugin = PluginLoader::load("oauth2");
    if (Q_UNLIKELY(!m_plugin)) {
        q->setError(AbstractAuthenticator::InternalError,
                    QStringLiteral("Couldn't load OAuth plugin"));
        Q_EMIT q->finished();
        return false;
    }

    QObject::connect(m_plugin, &AuthPluginInterface::result,
                     this, &OAuth1Private::onResult);
    QObject::connect(m_plugin, &AuthPluginInterface::userActionRequired,
                     this, &OAuth1Private::onUserActionRequired);
    QObject::connect(m_plugin, &AuthPluginInterface::store,
                     this, &AbstractAuthenticatorPrivate::handleStore);
    QObject::connect(m_plugin, &AuthPluginInterface::error,
                     this, &OAuth1Private::onError);

    return true;
}

void OAuth1Private::process()
{
    if (!ensurePlugin()) return;

    if (!m_userAgent.isEmpty()) {
        m_sessionData.setUserAgent(m_userAgent);
    }
    SignOn::SessionData sessionData(m_sessionData);
    SignOn::SessionData data(storedData());
    sessionData += data;
    m_plugin->process(sessionData, m_mechanism);
}

void OAuth1Private::actionCompleted(const QUrl &url)
{
    SignOn::UiSessionData data;
    data.setUrlResponse(url.toString());
    m_plugin->userActionFinished(data);

    AbstractAuthenticatorPrivate::actionCompleted(url);
}

void OAuth1Private::actionRejected()
{
    SignOn::UiSessionData data;
    data.setQueryErrorCode(SignOn::QUERY_ERROR_CANCELED);
    m_plugin->userActionFinished(data);

    AbstractAuthenticatorPrivate::actionRejected();
}

void OAuth1Private::onResult(const SignOn::SessionData &data)
{
    Q_Q(OAuth1);

    m_reply = static_cast<const OAuth2PluginNS::OAuth1PluginTokenData&>(data);
    q->finished();
}

void OAuth1Private::onUserActionRequired(const SignOn::UiSessionData &data)
{
    handleUserActionRequired(data);
}

void OAuth1Private::onError(const SignOn::Error &err)
{
    Q_Q(OAuth1);

    AbstractAuthenticator::ErrorCode code = AbstractAuthenticator::InternalError;

    switch (err.type()) {
    case SignOn::Error::MissingData:
        code = AbstractAuthenticator::MissingData;
        break;
    case SignOn::Error::InvalidCredentials:
    case SignOn::Error::NotAuthorized:
    case SignOn::Error::PermissionDenied:
        code = AbstractAuthenticator::NotAuthorized;
        break;
    case SignOn::Error::OperationFailed:
        code = AbstractAuthenticator::ServerError;
        break;
    }

    q->setError(code, err.message());
    Q_EMIT q->finished();
}

bool OAuth1Private::buildRequest(AuthenticatedRequest &request,
                                 const QUrl &url,
                                 const QByteArray &verb,
                                 const QVariantMap &params) const
{
    bool bodyless = (verb == "GET" || verb == "HEAD");

    if (!parseParameters(request, url, verb, params)) return false;

    QUrlQuery query(request.url);
    request.url = request.url.adjusted(QUrl::RemoveQuery);
    if (request.authorizationTransmission == AbstractAuthenticator::ParametersInQuery) {
        request.url.setQuery(signedQuery(request.url, verb, query));
    } else if (request.authorizationTransmission == AbstractAuthenticator::ParametersInHeader) {
        QUrlQuery remaining;
        QByteArray authHeader =
            authorizationHeader(signedQuery(request.url, verb, query),
                                remaining);
        request.headers.append({ "Authorization", authHeader });
        request.url.setQuery(remaining);
    } else if (request.authorizationTransmission == AbstractAuthenticator::ParametersInBody) {
        if (bodyless) {
            qWarning() << "Can't send OAuth params in body (GET request)!";
            return false;
        }
        if (request.multiPart) {
            addPartsFromQuery(request.multiPart, signedQuery(request.url, verb, query));
        } else {
            QString queryString =
                signedQuery(request.url, verb, query).toString();
            request.body = queryString.toUtf8();
        }
    } else {
        qWarning() << "Invalid OAuth params transmission:"
            << request.authorizationTransmission;
        return false;
    }

    if (!request.multiPart && !request.body.isEmpty()) {
        request.headers.append({
            "Content-Type", QByteArrayLiteral("application/x-www-form-urlencoded")
        });
    }
    return true;
}

QUrlQuery OAuth1Private::signedQuery(const QUrl &url,
                                     const QByteArray &verb,
                                     const QUrlQuery &params) const
{
    QUrlQuery query(params);
    query.addQueryItem("oauth_signature_method", m_mechanism);
    query.addQueryItem("oauth_version", "1.0");
    query.addQueryItem("oauth_consumer_key", m_sessionData.ConsumerKey());
    query.addQueryItem("oauth_token", m_reply.AccessToken());
#ifdef BUILDING_TESTS
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QString timestamp =
        env.value("LIBAUTHENTICATION_TEST_oauth_timestamp", "1420534672");
    QString nonce = env.value("LIBAUTHENTICATION_TEST_oauth_nonce", "32481369");
#else
    QString timestamp =
        QString::number(QDateTime::currentDateTime().toTime_t());
    QString nonce = QString::number(qrand());
#endif
    query.addQueryItem("oauth_timestamp", timestamp);
    query.addQueryItem("oauth_nonce", nonce);

    QByteArray signature = signRequest(url, verb, query);
    query.addQueryItem("oauth_signature", signature);

    return query;
}

QByteArray OAuth1Private::authorizationHeader(const QUrlQuery &signedQuery,
                                              QUrlQuery &params) const
{
    QByteArray header("OAuth ");
    bool first = true;
    params = signedQuery;
    const auto items = params.queryItems(QUrl::FullyEncoded);
    for (const auto &pair: items) {
        if (pair.first.startsWith("oauth_") ||
            pair.first == QLatin1String("realm")) {
            if (!first) {
                header += ", ";
            } else {
                first = false;
            }
            header.append(pair.first + "=\"" + pair.second + '"');
            params.removeQueryItem(pair.first);
        }
    }
    return header;
}

QByteArray OAuth1Private::signRequest(const QUrl &requestUrl,
                                      const QByteArray &verb,
                                      const QUrlQuery &query) const
{
    QByteArray baseString = verb;

    baseString += '&';
    baseString += QUrl::toPercentEncoding(requestUrl.toString());
    // Query parameters must be sorted before signing
    QList<QPair<QString, QString>> items =
        query.queryItems(QUrl::FullyDecoded);
    std::sort(items.begin(), items.end());
    QByteArray queryString;
    Q_FOREACH(const auto &pair, items) {
        if (!queryString.isEmpty()) queryString += '&';
        queryString += QUrl::toPercentEncoding(pair.first) +
            '=' + QUrl::toPercentEncoding(pair.second);
    }
    baseString += '&' + QUrl::toPercentEncoding(queryString);

    QByteArray key = m_sessionData.ConsumerSecret().toLatin1() +
        '&' + m_reply.TokenSecret().toLatin1();
    return QUrl::toPercentEncoding(hashHMACSHA1(baseString, key).toBase64());
}

// Create a HMAC-SHA1 signature
QByteArray OAuth1Private::hashHMACSHA1(const QByteArray &baseString,
                                       const QByteArray &secret) const
{
    // The algorithm is defined in RFC 2104
    int blockSize = 64;
    QByteArray key(secret);
    QByteArray opad(blockSize, 0x5c);
    QByteArray ipad(blockSize, 0x36);

    // If key size is too large, compute the hash
    if (key.size() > blockSize) {
        key = QCryptographicHash::hash(key, QCryptographicHash::Sha1);
    }

    // If too small, pad with 0x00
    if (key.size() < blockSize) {
        key += QByteArray(blockSize - key.size(), 0x00);
    }

    // Compute the XOR operations
    for (int i = 0; i <= key.size() - 1; i++) {
        ipad[i] = (char) (ipad[i] ^ key[i]);
        opad[i] = (char) (opad[i] ^ key[i]);
    }

    // Append the data to ipad
    ipad += baseString;

    // Hash sha1 of ipad and append the data to opad
    opad += QCryptographicHash::hash(ipad, QCryptographicHash::Sha1);

    // Return array contains the result of HMAC-SHA1
    return QCryptographicHash::hash(opad, QCryptographicHash::Sha1);
}

OAuth1::OAuth1(QObject *parent):
    AbstractAuthenticator(new OAuth1Private(this), parent)
{
    /* Workaround a moc limitation: when defining QObject properties, moc will
     * complain if the notify signal is not found in the very same class which
     * defines the property. So, we need to relay the finished signal on this
     * class too.
     */
    QObject::connect(this, &AbstractAuthenticator::finished,
                     this, &OAuth1::finished);
}

OAuth1::~OAuth1()
{
}

void OAuth1::process()
{
    Q_D(OAuth1);
    setError(NoError, QString());
    d->process();
}

void OAuth1::logout()
{
    Q_D(OAuth1);
    d->m_reply = OAuth2PluginNS::OAuth1PluginTokenData();
    AbstractAuthenticator::logout();
}

void OAuth1::setClientId(const QString &clientId)
{
    Q_D(OAuth1);
    d->m_sessionData.setConsumerKey(clientId);
    Q_EMIT clientIdChanged();
}

QString OAuth1::clientId() const
{
    Q_D(const OAuth1);
    return d->m_sessionData.ConsumerKey();
}

void OAuth1::setClientSecret(const QString &clientSecret)
{
    Q_D(OAuth1);
    d->m_sessionData.setConsumerSecret(clientSecret);
    Q_EMIT clientSecretChanged();
}

QString OAuth1::clientSecret() const
{
    Q_D(const OAuth1);
    return d->m_sessionData.ConsumerSecret();
}

void OAuth1::setTemporaryCredentialsUrl(const QUrl &temporaryCredentialsUrl)
{
    Q_D(OAuth1);
    d->m_sessionData.setRequestEndpoint(temporaryCredentialsUrl.toString());
    Q_EMIT temporaryCredentialsUrlChanged();
}

QUrl OAuth1::temporaryCredentialsUrl() const
{
    Q_D(const OAuth1);
    return QUrl(d->m_sessionData.RequestEndpoint());
}

void OAuth1::setAuthorizationUrl(const QUrl &authorizationUrl)
{
    Q_D(OAuth1);
    d->m_sessionData.setAuthorizationEndpoint(authorizationUrl.toString());

    /* Additionally, use the domain as account ID; TODO: let the user
     * specify the account ID */
    setAccountId(authorizationUrl.host());

    Q_EMIT authorizationUrlChanged();
}

QUrl OAuth1::authorizationUrl() const
{
    Q_D(const OAuth1);
    return QUrl(d->m_sessionData.AuthorizationEndpoint());
}

void OAuth1::setTokenRequestUrl(const QUrl &tokenRequestUrl)
{
    Q_D(OAuth1);
    d->m_sessionData.setTokenEndpoint(tokenRequestUrl.toString());
    Q_EMIT tokenRequestUrlChanged();
}

QUrl OAuth1::tokenRequestUrl() const
{
    Q_D(const OAuth1);
    return QUrl(d->m_sessionData.TokenEndpoint());
}

void OAuth1::setSignatureMethod(const QString &signatureMethod)
{
    Q_D(OAuth1);
    d->m_mechanism = signatureMethod;
    Q_EMIT signatureMethodChanged();
}

QString OAuth1::signatureMethod() const
{
    Q_D(const OAuth1);
    return d->m_mechanism;
}

void OAuth1::setRealm(const QString &realm)
{
    Q_D(OAuth1);
    d->m_sessionData.setRealm(realm);
    Q_EMIT realmChanged();
}

QString OAuth1::realm() const
{
    Q_D(const OAuth1);
    return d->m_sessionData.Realm();
}

void OAuth1::setCallbackUrl(const QString &callbackUrl)
{
    Q_D(OAuth1);
    d->m_sessionData.setCallback(callbackUrl);
    Q_EMIT callbackUrlChanged();
}

QString OAuth1::callbackUrl() const
{
    Q_D(const OAuth1);
    return d->m_sessionData.Callback();
}

QByteArray OAuth1::token() const
{
    Q_D(const OAuth1);
    return d->m_reply.AccessToken().toUtf8();
}

QByteArray OAuth1::tokenSecret() const
{
    Q_D(const OAuth1);
    return d->m_reply.TokenSecret().toUtf8();
}

void OAuth1::injectTokens(const QByteArray &token,
                          const QByteArray &tokenSecret)
{
    Q_D(OAuth1);
    d->m_reply.setAccessToken(token);
    d->m_reply.setTokenSecret(tokenSecret);
    Q_EMIT finished(); // notification of property change
}

#include "oauth1.moc"
