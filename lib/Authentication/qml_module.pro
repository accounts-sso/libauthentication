include(../../common-config.pri)

TEMPLATE = lib
TARGET = qml_plugin

API_URI = "Authentication"
API_VER = 0.1

MODULE_DIR = $$replace(API_URI, \\., /)
PLUGIN_INSTALL_BASE = $$[QT_INSTALL_QML]/$${MODULE_DIR}

CONFIG += \
    plugin \
    qt

QT += qml

# Error on undefined symbols
QMAKE_LFLAGS += $$QMAKE_LFLAGS_NOUNDEF

SOURCES = \
    qml_loopback_server.cpp \
    qml_plugin.cpp

HEADERS += \
    qml_loopback_server.h \
    qml_plugin.h

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib
QMAKE_LIBDIR = $${TOP_BUILD_DIR}/lib/Authentication$${BUILDD}
LIBS += -lAuthentication

QMLDIR_FILES += qmldir
QMAKE_SUBSTITUTES += qmldir.in
OTHER_FILES += qmldir.in

!CONFIG(noinstall_qtmodules) {
    target.path = $${PLUGIN_INSTALL_BASE}
    INSTALLS += target

    qmldir.files = qmldir
    qmldir.path = $${PLUGIN_INSTALL_BASE}
    INSTALLS += qmldir
}
