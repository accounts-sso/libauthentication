/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "multipart.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

using namespace Authentication;

namespace Authentication {

class FakeReply: public QNetworkReply
{
    Q_OBJECT
public:
    FakeReply(const QNetworkRequest &req, QNetworkAccessManager *parent):
        QNetworkReply(parent)
    {
        Q_UNUSED(req);
        QNetworkReply::open(QIODevice::ReadOnly);
    }
    void abort() override {}
    qint64 readData(char *, qint64) override { return 0; }
};

class MultipartPrivate: public QNetworkAccessManager
{
    Q_OBJECT

    using QNetworkAccessManager::QNetworkAccessManager;

    QNetworkReply *createRequest(Operation op, const QNetworkRequest &req,
                                 QIODevice *outgoingData) override {
        Q_UNUSED(op);
        m_data = outgoingData->readAll();
        return new FakeReply(req, this);
    }

public:
    QByteArray m_data;
};

} // namespace

Multipart::Multipart():
    d_ptr(new MultipartPrivate)
{
}

Multipart::~Multipart()
{
}

QByteArray Multipart::toByteArray(QHttpMultiPart *multiPart)
{
    Q_D(Multipart);

    QNetworkRequest dummy(QUrl("http://multipart.test"));
    delete d->post(dummy, multiPart);
    return d->m_data;
}

#include "multipart.moc"
