/*
 * This file is part of signon
 *
 * Copyright (C) 2017 mardy.it
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef SIGNON_PLUGIN_LOADER_H
#define SIGNON_PLUGIN_LOADER_H

#include <QScopedPointer>
#include <QString>

#include <SignOn/global.h>

class AuthPluginInterface;

namespace SignOn {

class PluginLoaderPrivate;
class SIGNON_EXPORT PluginLoader
{
public:
    PluginLoader();
    virtual ~PluginLoader();

    AuthPluginInterface *load(const QString &baseName) const;

private:
    QScopedPointer<PluginLoaderPrivate> d_ptr;
    Q_DECLARE_PRIVATE(PluginLoader);
};

} // namespace

#endif // SIGNON_PLUGIN_LOADER_H
