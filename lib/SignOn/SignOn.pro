TEMPLATE = lib
TARGET = signon-plugins

include(../../common-config.pri)

INCLUDEPATH += \
    ..

DEFINES += \
    BUILDING_SIGNON_PLUGINS

SOURCES += \
    plugin_loader.cpp

public_headers += \
    authpluginif.h AuthPluginInterface \
    global.h \
    plugin_loader.h PluginLoader \
    signonerror.h Error \
    sessiondata.h SessionData \
    uisessiondata_priv.h \
    uisessiondata.h UiSessionData

HEADERS += $$public_headers $$private_headers

headers.files = $$public_headers
headers.path = $$INSTALL_PREFIX/include/$$TARGET/SignOn
INSTALLS += headers

pkgconfig.files = $${TARGET}.pc
include($${TOP_SRC_DIR}/common-pkgconfig.pri)
INSTALLS += pkgconfig

target.path = $${INSTALL_LIBDIR}
INSTALLS += target
