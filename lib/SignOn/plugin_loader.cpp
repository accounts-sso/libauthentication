/*
 * This file is part of signon
 *
 * Copyright (C) 2017 mardy.it
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "plugin_loader.h"

#include <QDebug>
#include <QObject>
#include <QPluginLoader>
#include "AuthPluginInterface"

using namespace SignOn;

namespace SignOn {

class PluginLoaderPrivate {
};

} // namespace

PluginLoader::PluginLoader():
    d_ptr(0)
{
}

PluginLoader::~PluginLoader()
{
}

AuthPluginInterface *PluginLoader::load(const QString &baseName) const
{
    QPluginLoader loader(baseName + QStringLiteral("plugin"));
    AuthPluginInterface2 *interface =
        qobject_cast<AuthPluginInterface2 *>(loader.instance());
    if (!interface) {
        qWarning() << "Could not load plugin" << baseName;
        return 0;
    }

    return interface->createAuthPlugin();
}
