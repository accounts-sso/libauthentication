TEMPLATE = subdirs

signon.subdir = lib/SignOn
signon.target = sub-SignOn

authentication.subdir = lib/Authentication

SUBDIRS = \
    signon \
    authentication \
    plugins

plugins.depends = signon

!CONFIG(nomake_tests) {
    SUBDIRS += tests
    tests.depends = \
        authentication \
        signon
}

!CONFIG(nomake_examples) {
    SUBDIRS += examples
    examples.depends = \
        authentication \
        plugins
}

include(common-config.pri)
