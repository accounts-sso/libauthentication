[![pipeline status](https://gitlab.com/accounts-sso/libauthentication/badges/master/pipeline.svg)](https://gitlab.com/accounts-sso/libauthentication/commits/master)
[![coverage report](https://gitlab.com/accounts-sso/libauthentication/badges/master/coverage.svg)](http://accounts-sso.gitlab.io/libauthentication/coverage/)

In-process authentication library
=================================

This library provides a way to perform the [OAuth 1.0][1] and [OAuth 2.0][2]
authentication flows to applications using the Qt toolkit.

Unlike most of other projects under the [Accounts & SSO][3] umbrella, this
library does not depend on D-Bus and does not require spawning of child
processes: everything happens in your application's process. This makes it
possible to use this library on multi-platform applications; for example,
[PhotoTeleport](https://www.phototeleport.com) uses libauthentication and is
available for Linux, Mac and Windows.


License
-------

See the LICENSE file.


Build instructions
------------------

The only dependency of libauthentication is Qt 5.6 (or later); and specifically
the Core and Network submodules of QtBase. The recommended way to build the
library is the following:
```
# Make sure you have checkout the plugin submodules:
git submodule init && git submodule update
mkdir build
cd build
qmake ..
make
sudo make install
```

    

[1]: https://tools.ietf.org/html/rfc5849
[2]: https://tools.ietf.org/html/rfc6749
[3]: https://accounts-sso.gitlab.io/
