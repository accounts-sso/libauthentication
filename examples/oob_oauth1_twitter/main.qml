import QtQuick 2.0
import Authentication 0.1

Item {
    id: root

    width: 400
    height: 600
    state: "startUp"

    property var request: null

    Rectangle {
        id: loginButton

        anchors.centerIn: parent
        width: 200
        height: 50
        color: "lightsteelblue"

        Text {
            anchors.fill: parent
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Login")
        }

        MouseArea {
            anchors.fill: parent
            onClicked: oauth.process()
        }
    }

    Column {
        id: codeInput
        anchors.centerIn: parent
        width: 200

        Text {
            anchors { left: parent.left; right: parent.right }
            wrapMode: Text.WordWrap
            text: qsTr("Enter the code shown in your browser:")
        }

        TextInput {
            anchors { left: parent.left; right: parent.right }
            onAccepted: {
                // we can send any url, what matters is the query string
                root.request.setResult("https://anyurl.com/?oauth_verifier=" + text)
            }

            Rectangle {
                anchors { fill: parent; margins: -1 }
                color: "transparent"
                border { width: 1; color: "lightsteelblue" }
            }
        }
    }

    OAuth1 {
        id: oauth
        clientId: "NGOB5S7sICsj6epjh0PhAw"
        clientSecret: "rbUEJCBEokMnGZd8bubd0QL2cSmoCjJeyiSJpnx3OM0"
        temporaryCredentialsUrl: "https://api.twitter.com/oauth/request_token"
        authorizationUrl: "https://api.twitter.com/oauth/authorize"
        tokenRequestUrl: "https://api.twitter.com/oauth/access_token"
        callbackUrl: "oob"
        signatureMethod: "HMAC-SHA1"
        authorizationTransmission: OAuth1.ParametersInHeader

        onFinished: {
            console.log("Authentication completed")
            console.log("Has error: " + hasError)
            if (hasError) {
                console.log("Error (" + errorCode + "): " + errorMessage)
            }
            console.log("Access token: " + token)

            uploadFile("/usr/share/backgrounds/space-01.jpg")
        }
        onActionRequested: {
            console.log("Request url: " + request.url)
            Qt.openUrlExternally(request.url)
            root.request = request
        }

        function uploadFile(filePath) {
            var http = newPostRequest("https://upload.twitter.com/1.1/media/upload.json", {
                "media_data": {
                    "filePath": filePath,
                    "fileName": "JustAnImage.jpg",
                    "contentType": "image/jpeg",
                    "base64": true,
                }
            })
            http.onreadystatechange = function() {
                if (http.readyState === 4){
                    console.log("upload response text: " + http.responseText)
                    var mediaId=""
                    if (http.status == 200 || http.status == 202) {
                        console.log("ok")
                        var response = JSON.parse(http.responseText)
                        mediaId = response.media_id_string
                    } else {
                        console.log("error: " + http.status)
                    }
                }
            };
            http.send(http.body)
        }
    }

    states: [
        State {
            name: "startUp"
            PropertyChanges { target: loginButton; visible: true }
            PropertyChanges { target: codeInput; visible: false }
        },
        State {
            name: "needsCode"
            when: root.request != null
            PropertyChanges { target: loginButton; visible: false }
            PropertyChanges { target: codeInput; visible: true }
        }
    ]
}
