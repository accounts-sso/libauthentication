import QtQuick 2.0
import Authentication 0.1

Item {
    id: root

    width: 400
    height: 300

    Rectangle {
        id: button

        anchors.centerIn: parent
        width: 200
        height: 50
        color: "lightsteelblue"

        Text {
            anchors.fill: parent
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Login")
        }

        MouseArea {
            anchors.fill: parent
            onClicked: oauth.process()
        }
    }

    Loader {
        id: webviewLoader
        anchors.fill: parent
    }

    OAuth1 {
        id: oauth
        clientId: "d87224f0b467093b2a87fd788d950e27"
        clientSecret: "4c7e48102c226509"
        temporaryCredentialsUrl: "https://secure.flickr.com/services/oauth/request_token"
        authorizationUrl: "https://secure.flickr.com/services/oauth/authorize"
        tokenRequestUrl: "https://secure.flickr.com/services/oauth/access_token"
        callbackUrl: "https://wiki.ubuntu.com/"
        signatureMethod: "HMAC-SHA1"
        authorizationTransmission: OAuth1.ParametersInQuery

        onFinished: {
            console.log("Authentication completed")
            console.log("Has error: " + hasError)
            if (hasError) {
                console.log("Error (" + errorCode + "): " + errorMessage)
            }
            webviewLoader.active = false
            console.log("Access token: " + token)

            var http = newGetRequest("https://api.flickr.com/services/rest/?format=json", {
                "method": "flickr.test.login",
                "nojsoncallback": "1",
            })
                //"format": "json",
            http.onreadystatechange = function() {
                if (http.readyState === 4){
                    console.log("response text: " + http.responseText)
                    if (http.status == 200) {
                        console.log("ok")
                        var response = JSON.parse(http.responseText)
                    } else {
                        console.log("error: " + http.status)
                    }
                }
            };
            http.send()
        }
        onActionRequested: {
            console.log("Request url: " + request.url)
            webviewLoader.setSource(Qt.resolvedUrl("Webview.qml"), {
                "request": request,
            })
        }
    }
}
