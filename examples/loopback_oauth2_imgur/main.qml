import QtQuick 2.0
import Authentication 0.2

Item {
    id: root

    width: 400
    height: 300
    state: "startUp"

    property var request: null
    property string uploadedItem

    Rectangle {
        id: loginButton

        anchors.centerIn: parent
        width: 200
        height: 50
        color: "lightsteelblue"

        Text {
            anchors.fill: parent
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Login")
        }

        MouseArea {
            anchors.fill: parent
            onClicked: oauth.process()
        }
    }

    Text {
        id: uploadMessage
        anchors.centerIn: parent
        width: 300
        wrapMode: Text.WordWrap
        text: qsTr('Your image has been uploaded, you can see it <a href="%1">here</a>.').arg(root.uploadedItem)
        onLinkActivated: Qt.openUrlExternally(link)
    }

    LoopbackServer {
        id: httpServer
        port: 8080
        onVisited: root.request.setResult(url)
    }

    OAuth2 {
        id: oauth
        clientId: "f7eaa49b47b5b84"
        clientSecret: "7c99a05e7d988e9e760038bc5192bfc1a6390c8f"
        scopes: []
        authorizationUrl: "https://api.imgur.com/oauth2/authorize"
        accessTokenUrl: "https://api.imgur.com/oauth2/token"
        responseType: "code"
        callbackUrl: httpServer.callbackUrl

        onFinished: {
            console.log("Authentication completed")
            console.log("Has error: " + hasError)
            if (hasError) {
                console.log("Error (" + errorCode + "): " + errorMessage)
            }
            console.log("Access token: " + accessToken)

            var http = newGetRequest("https://api.imgur.com/3/account/me", {})
            http.onreadystatechange = function() {
                if (http.readyState === 4){
                    if (http.status == 200) {
                        console.log("ok")
                        var response = JSON.parse(http.responseText)
                    } else {
                        console.log("error: " + http.status)
                    }
                    console.log("response text: " + http.responseText)
                    uploadFile("/usr/share/backgrounds/space-01.jpg")
                }
            };
            http.send()
        }
        onActionRequested: {
            console.log("Request url: " + request.url)
            Qt.openUrlExternally(request.url)
            root.request = request
        }

        function uploadFile(filePath) {
            var http = newPostRequest("https://api.imgur.com/3/image", {
                "image": {
                    "filePath": filePath,
                    "fileName": "JustAnImage.jpg",
                    "contentType": "image/jpeg",
                },
                "type": "file",
            })
            http.onreadystatechange = function() {
                if (http.readyState === XMLHttpRequest.DONE){
                    if (http.status >= 200 && http.status < 300) {
                        console.log("ok")
                        var response = JSON.parse(http.responseText)
                        root.uploadedItem = response.data.link
                    } else {
                        console.log("error: " + http.status)
                        console.log(http.responseText)
                    }
                }
            }
            http.send(http.body)
        }
    }

    states: [
        State {
            name: "startUp"
            PropertyChanges { target: loginButton; visible: true }
            PropertyChanges { target: uploadMessage; visible: false }
        },
        State {
            name: "needsCode"
            when: root.uploadedItem
            PropertyChanges { target: loginButton; visible: false }
            PropertyChanges { target: uploadMessage; visible: true }
        }
    ]
}
