import QtQuick 2.0
import Authentication 0.1

Item {
    id: root

    width: 400
    height: 300

    Rectangle {
        id: button

        anchors.centerIn: parent
        width: 200
        height: 50
        color: "lightsteelblue"

        Text {
            anchors.fill: parent
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Login")
        }

        MouseArea {
            anchors.fill: parent
            onClicked: oauth.process()
        }
    }

    Loader {
        id: webviewLoader
        anchors.fill: parent
    }

    OAuth2 {
        id: oauth
        clientId: "759250720802-4sii0me9963n9fdqdmi7cepn6ub8luoh.apps.googleusercontent.com"
        clientSecret: "juFngKUcuhB7IRQqHtSLavqJ"
        scopes: [ "https://www.googleapis.com/auth/userinfo.profile" ]
        authorizationUrl: "https://accounts.google.com/o/oauth2/auth"
        accessTokenUrl: "https://accounts.google.com/o/oauth2/token"
        responseType: "code"
        callbackUrl: "https://wiki.ubuntu.com/"
        protocolTweaks: OAuth2.ClientAuthInRequestBody

        onFinished: {
            console.log("Authentication completed")
            console.log("Has error: " + hasError)
            if (hasError) {
                console.log("Error (" + errorCode + "): " + errorMessage)
            }
            webviewLoader.active = false
            console.log("Access token: " + accessToken)

            var http = newGetRequest("https://www.googleapis.com/oauth2/v3/userinfo", {})
            http.onreadystatechange = function() {
                if (http.readyState === 4){
                    if (http.status == 200) {
                        console.log("ok")
                        var response = JSON.parse(http.responseText)
                    } else {
                        console.log("error: " + http.status)
                    }
                    console.log("response text: " + http.responseText)
                }
            };
            http.send()
        }
        onActionRequested: {
            console.log("Request url: " + request.url)
            webviewLoader.setSource(Qt.resolvedUrl("Webview.qml"), {
                "request": request,
            })
        }
    }
}
