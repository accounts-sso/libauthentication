TEMPLATE = subdirs
SUBDIRS = \
    cpp_webengine \
    loopback_oauth2_imgur \
    oob_oauth1_twitter \
    qml_webview \
    webengine_oauth1_flickr \
    webview_oauth1_flickr
