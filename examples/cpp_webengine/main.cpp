/*
 * This file is part of signon
 *
 * Copyright (C) 2017 mardy.it
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <Authentication/OAuth2>
#include <QApplication>
#include <QLabel>
#include <QMainWindow>
#include <QPushButton>
#include <QStackedLayout>
#include <QUrl>
#include <QVBoxLayout>
#include <QWebEngineView>

class MainWindow: public QMainWindow
{
    Q_OBJECT

    enum Pages {
        LoginPage = 0,
        WebViewPage,
        ResultPage,
    };

public:
    MainWindow() {
        setCentralWidget(new QWidget);
        m_stack = new QStackedLayout(centralWidget());

        QPushButton *loginButton = new QPushButton("Login");
        QObject::connect(loginButton, &QPushButton::clicked,
                         this, &MainWindow::loginPressed);
        QLayout *layout = new QVBoxLayout;
        layout->addWidget(loginButton);
        QWidget *loginPage = new QWidget;
        loginPage->setLayout(layout);
        m_stack->addWidget(loginPage);

        m_webView = new QWebEngineView;
        QObject::connect(m_webView, &QWebEngineView::urlChanged,
                         this, &MainWindow::onUrlChanged);
        m_stack->addWidget(m_webView);

        m_tokenLabel = new QLabel;
        layout = new QVBoxLayout;
        layout->addWidget(m_tokenLabel);
        QWidget *resultPage = new QWidget;
        resultPage->setLayout(layout);
        m_stack->addWidget(resultPage);

    }

    void handleRequest(Authentication::ActionRequest request) {
        m_request = request;
        m_webView->load(request.url());
        m_stack->setCurrentIndex(WebViewPage);
    }

    void onUrlChanged(const QUrl &url) {
        QUrl finalUrl = m_request.finalUrl();
        if (url.host() == finalUrl.host() &&
            url.path() == finalUrl.path()) {
            m_request.setResult(url);
        }
    }

    void showToken(const QByteArray &accessToken) {
        m_tokenLabel->setText(QString::fromLatin1(accessToken));
        m_stack->setCurrentIndex(ResultPage);
    }

Q_SIGNALS:
    void loginPressed();
    void finalUrlReached();

private:
    QStackedLayout *m_stack;
    QWebEngineView *m_webView;
    QLabel *m_tokenLabel;
    Authentication::ActionRequest m_request;
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    app.addLibraryPath(OAUTH_PLUGIN_DIR);

    Authentication::OAuth2 oauth2;

    oauth2.setClientId("759250720802-4sii0me9963n9fdqdmi7cepn6ub8luoh.apps.googleusercontent.com");
    oauth2.setClientSecret("juFngKUcuhB7IRQqHtSLavqJ");
    oauth2.setScopes({ "https://www.googleapis.com/auth/userinfo.profile" });
    oauth2.setAuthorizationUrl(QUrl("https://accounts.google.com/o/oauth2/auth"));
    oauth2.setAccessTokenUrl(QUrl("https://accounts.google.com/o/oauth2/token"));
    oauth2.setResponseType("code");
    oauth2.setCallbackUrl("https://wiki.ubuntu.com/");
    oauth2.setProtocolTweaks(Authentication::OAuth2::ClientAuthInRequestBody);

    MainWindow window;
    QObject::connect(&window, &MainWindow::loginPressed,
                     &oauth2, &Authentication::OAuth2::process);
    QObject::connect(&oauth2, &Authentication::OAuth2::actionRequested,
                     &window, &MainWindow::handleRequest);
    QObject::connect(&oauth2, &Authentication::OAuth2::finished,
                     [&]() {
        window.showToken(oauth2.accessToken());
    });
    window.show();

    return app.exec();
}

#include "main.moc"
