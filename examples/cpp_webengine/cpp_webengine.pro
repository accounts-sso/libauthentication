TEMPLATE = app
TARGET = cpp_webengine

include(../../common-config.pri)

QT += \
    webenginewidgets \
    widgets

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib

LIBS += -lAuthentication
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/Authentication

LIBS += -lsignon-plugins
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/SignOn

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

DEFINES += \
    OAUTH_PLUGIN_DIR=\\\"$${TOP_BUILD_DIR}/plugins/signon-oauth2/src/lib/signon/\\\" \
    QML_FILE_DIR=\\\"$${PWD}/\\\"

SOURCES += \
    main.cpp
