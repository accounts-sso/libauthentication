include(../common-config.pri)

CONFIG += \
    c++11 \
    no_keywords \
    qt

QT += \
    core \
    network \
    testlib

DEFINES += \
    BUILDING_TESTS

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib

HEADERS += \
    test_utils.h

OBJECTS_DIR = $${TARGET}.obj
MOC_DIR = $${OBJECTS_DIR}

check.commands = ./$${TARGET}
check.depends = $${TARGET}
QMAKE_EXTRA_TARGETS += check
