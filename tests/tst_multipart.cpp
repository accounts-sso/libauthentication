/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/multipart.h"
#include <QDebug>
#include <QFile>
#include <QHttpMultiPart>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Authentication;

class MultipartTest: public QObject
{
    Q_OBJECT

public:
    QByteArray addBoundaries(const QByteArray &data,
                             const QHttpMultiPart *multiPart);

private Q_SLOTS:
    void testSinglePart();
    void testUpload();
};

QByteArray MultipartTest::addBoundaries(const QByteArray &data,
                                        const QHttpMultiPart *multiPart)
{
    QByteArray copy(data);
    return copy.replace("BOUNDARY", multiPart->boundary());
}

void MultipartTest::testSinglePart()
{
    Multipart reader;

    QScopedPointer<QHttpMultiPart> multiPart(
        new QHttpMultiPart(QHttpMultiPart::FormDataType));

    QHttpPart part;
    part.setHeader(QNetworkRequest::ContentDispositionHeader,
                   "form-data; name=\"field\"");
    part.setBody("Some entertaining text");
    multiPart->append(part);

    QByteArray expectedData = "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"field\"\r\n"
        "\r\n"
        "Some entertaining text\r\n"
        "--BOUNDARY--\r\n";

    QCOMPARE(reader.toByteArray(multiPart.data()),
             addBoundaries(expectedData, multiPart.data()));
}

void MultipartTest::testUpload()
{
    Multipart reader;

    QFile file(TEST_FILE);
    file.open(QIODevice::ReadOnly);

    QScopedPointer<QHttpMultiPart> multiPart(
        new QHttpMultiPart(QHttpMultiPart::FormDataType));

    QHttpPart part;
    part.setHeader(QNetworkRequest::ContentDispositionHeader,
                   "form-data; name=\"story\"");
    part.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain");
    part.setBodyDevice(&file);
    multiPart->append(part);

    QByteArray expectedData = "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"story\"\r\n"
        "Content-Type: text/plain\r\n"
        "\r\n"
        "Story\n"
        "=====\n"
        "\n"
        "This is a very sad story. It begins here,\n"
        "and it ends here.\n"
        "\r\n"
        "--BOUNDARY--\r\n";

    QCOMPARE(reader.toByteArray(multiPart.data()),
             addBoundaries(expectedData, multiPart.data()));
}

QTEST_GUILESS_MAIN(MultipartTest)

#include "tst_multipart.moc"
