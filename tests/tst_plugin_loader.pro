TARGET = tst_plugin_loader

include(authentication_tests.pri)

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib/SignOn

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${AUTHENTICATION_SRC}/plugin_loader.cpp \
    tst_plugin_loader.cpp

HEADERS += \
    $${AUTHENTICATION_SRC}/plugin_loader.h
