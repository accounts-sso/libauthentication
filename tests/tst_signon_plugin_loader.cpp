/*
 * This file is part of libAuthentication.
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <QCoreApplication>
#include <QDebug>
#include <QScopedPointer>
#include <QTest>
#include "SignOn/AuthPluginInterface"
#include "SignOn/PluginLoader"

using namespace SignOn;

class PluginLoaderTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testLoad();
};

void PluginLoaderTest::testLoad()
{
    QScopedPointer<PluginLoader> loader(new PluginLoader);

    QTest::ignoreMessage(QtWarningMsg, "Could not load plugin \"non-existing\"");

    QScopedPointer<AuthPluginInterface> interface(loader->load("non-existing"));
    QVERIFY(!interface);

    QCoreApplication::addLibraryPath(PLUGIN_DIR);
    interface.reset(loader->load("test"));
    QVERIFY(interface);

    QCOMPARE(interface->metaObject()->className(), "TestPlugin::Plugin");
}

QTEST_GUILESS_MAIN(PluginLoaderTest)

#include "tst_signon_plugin_loader.moc"
