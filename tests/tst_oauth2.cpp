/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/AbstractAuthenticator"
#include "Authentication/ActionRequest"
#include "Authentication/OAuth2"
#include "Authentication/plugin_loader.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <SignOn/AuthPluginInterface>
#include "fake_network.h"
#include "test_utils.h"

using namespace Authentication;

/* Fake OAuth plugin */

class MockPlugin;
static MockPlugin *mockPlugin = 0;

class MockPlugin: public AuthPluginInterface
{
    Q_OBJECT
public:
    MockPlugin(const QString &baseName):
        m_type(baseName)
    {
        mockPlugin = this;
    }

    ~MockPlugin() { mockPlugin = 0; }

    QString type() const override { return m_type; }

    QStringList mechanisms() const override {
        return QStringList { "user_agent", "web_server" };
    }

    void process(const SignOn::SessionData &data,
                 const QString &mechanism) override {
        Q_EMIT processCalled(data.toMap(), mechanism);
    }

    void userActionFinished(const SignOn::UiSessionData &data) override {
        Q_EMIT userActionFinishedCalled(data.toMap());
    }

    void emitUserActionRequired(const QVariantMap &map) {
        Q_EMIT userActionRequired(SignOn::UiSessionData(map));
    }

    void emitResult(const QVariantMap &map) {
        Q_EMIT result(SignOn::SessionData(map));
    }

    void emitError(const SignOn::Error &err) {
        Q_EMIT error(err);
    }

Q_SIGNALS:
    void processCalled(const QVariantMap &map, const QString &mechanism);
    void userActionFinishedCalled(const QVariantMap &map);

private:
    QString m_type;
};

/* Mock plugin loader */

AuthPluginInterface *PluginLoader::load(const QString &baseName)
{
    return (mockPlugin && baseName == mockPlugin->type()) ? mockPlugin : 0;
}

/* Finally, the test class */

class OAuth2Test: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testGetters();
    void testNoPlugin();
    void testProcess_data();
    void testProcess();
    void testUserActionRequired_data();
    void testUserActionRequired();
    void testError_data();
    void testError();
    void testResult();
    void testGet_data();
    void testGet();
    void testGetRequest();
    void testLogout();
};

void OAuth2Test::testGetters()
{
    OAuth2 oauth2;

    oauth2.setClientId("aa 123");
    QCOMPARE(oauth2.clientId(), QString("aa 123"));

    oauth2.setClientSecret("&$%-as");
    QCOMPARE(oauth2.clientSecret(), QString("&$%-as"));

    QStringList scopes { "first", "second", "third" };
    oauth2.setScopes(scopes);
    QCOMPARE(oauth2.scopes(), scopes);

    QUrl authUrl("https://example.com/authorize");
    oauth2.setAuthorizationUrl(authUrl);
    QCOMPARE(oauth2.authorizationUrl(), authUrl);

    /* verify that the query is correctly handled */
    authUrl = QUrl("https://example.com/short/path?option=true");
    oauth2.setAuthorizationUrl(authUrl);
    QCOMPARE(oauth2.authorizationUrl(), authUrl);
    QVERIFY(oauth2.authorizationUrl().hasQuery());

    QUrl tokenUrl("https://example.com/tokenorize");
    oauth2.setAccessTokenUrl(tokenUrl);
    QCOMPARE(oauth2.accessTokenUrl(), tokenUrl);

    /* with query and port */
    tokenUrl = QUrl("https://site.com:678/path?really=true");
    oauth2.setAccessTokenUrl(tokenUrl);
    QCOMPARE(oauth2.accessTokenUrl(), tokenUrl);

    oauth2.setResponseType("code");
    QCOMPARE(oauth2.responseType(), QString("code"));

    oauth2.setCallbackUrl("oob");
    QCOMPARE(oauth2.callbackUrl(), QString("oob"));

    QCOMPARE(oauth2.protocolTweaks(), OAuth2::NoTweaks);
    oauth2.setProtocolTweaks(OAuth2::DisableStateParameter);
    QCOMPARE(oauth2.protocolTweaks(), OAuth2::DisableStateParameter);
    oauth2.setProtocolTweaks(OAuth2::ClientAuthInRequestBody);
    QCOMPARE(oauth2.protocolTweaks(), OAuth2::ClientAuthInRequestBody);
}

void OAuth2Test::testNoPlugin()
{
    OAuth2 oauth2;

    QSignalSpy finished(&oauth2, SIGNAL(finished()));
    oauth2.process();

    QCOMPARE(finished.count(), 1);
    QCOMPARE(oauth2.errorCode(), AbstractAuthenticator::InternalError);
    QCOMPARE(oauth2.errorMessage(), QString("Couldn't load OAuth plugin"));
}

void OAuth2Test::testProcess_data()
{
    QTest::addColumn<QString>("clientId");
    QTest::addColumn<QString>("clientSecret");
    QTest::addColumn<QStringList>("scopes");
    QTest::addColumn<QUrl>("authorizationUrl");
    QTest::addColumn<QUrl>("accessTokenUrl");
    QTest::addColumn<QString>("responseType");
    QTest::addColumn<QString>("callbackUrl");
    QTest::addColumn<int>("protocolTweaks");
    QTest::addColumn<QVariantMap>("expectedSessionData");
    QTest::addColumn<QString>("expectedMechanism");

    QTest::newRow("mostly empty") <<
        "" <<
        "" <<
        QStringList {} <<
        QUrl() <<
        QUrl() <<
        "other" <<
        QString() <<
        int(OAuth2::NoTweaks) <<
        QVariantMap {
            { "AuthPath", "" },
            { "ClientId", "" },
            { "ClientSecret", "" },
            { "DisableStateParameter", false },
            { "ForceClientAuthViaRequestBody", false },
            { "Host", "" },
            { "RedirectUri", "" },
            { "ResponseType", QStringList { "other" } },
            { "Scope", QStringList {} },
            { "TokenHost", "" },
            { "TokenPath", "" },
        } <<
        "";

    QTest::newRow("user_agent") <<
        "id123" <<
        "secret123" <<
        QStringList { "user_info", "friends" } <<
        QUrl("https://example.com:8080/authorize") <<
        QUrl("https://example.com:2020/token") <<
        "token" <<
        "http://localhost/cb" <<
        int(OAuth2::DisableStateParameter) <<
        QVariantMap {
            { "AuthPath", "authorize" },
            { "ClientId", "id123" },
            { "ClientSecret", "secret123" },
            { "DisableStateParameter", true },
            { "ForceClientAuthViaRequestBody", false },
            { "Host", "example.com:8080" },
            { "RedirectUri", "http://localhost/cb" },
            { "ResponseType", QStringList { "token" } },
            { "Scope", QStringList { "user_info", "friends" } },
            { "TokenHost", "example.com" },
            { "TokenPort", 2020 },
            { "TokenPath", "/token" },
        } <<
        "user_agent";

    QTest::newRow("web_server") <<
        "id123" <<
        "secret123" <<
        QStringList { "user_info", "friends" } <<
        QUrl("https://example.com/authorize") <<
        QUrl("https://other.com/token") <<
        "code" <<
        "oob" <<
        int(OAuth2::ClientAuthInRequestBody) <<
        QVariantMap {
            { "AuthPath", "authorize" },
            { "ClientId", "id123" },
            { "ClientSecret", "secret123" },
            { "DisableStateParameter", false },
            { "ForceClientAuthViaRequestBody", true },
            { "Host", "example.com" },
            { "RedirectUri", "oob" },
            { "ResponseType", QStringList { "code" } },
            { "Scope", QStringList { "user_info", "friends" } },
            { "TokenHost", "other.com" },
            { "TokenPath", "/token" },
        } <<
        "web_server";

    QTest::newRow("with query") <<
        "id123" <<
        "secret123" <<
        QStringList { "user_info", "friends" } <<
        QUrl("https://example.com/authorize?param=true") <<
        QUrl("https://other.com/token?value=123") <<
        "code" <<
        "oob" <<
        int(OAuth2::ClientAuthInRequestBody) <<
        QVariantMap {
            { "AuthPath", "authorize?param=true" },
            { "ClientId", "id123" },
            { "ClientSecret", "secret123" },
            { "DisableStateParameter", false },
            { "ForceClientAuthViaRequestBody", true },
            { "Host", "example.com" },
            { "RedirectUri", "oob" },
            { "ResponseType", QStringList { "code" } },
            { "Scope", QStringList { "user_info", "friends" } },
            { "TokenHost", "other.com" },
            { "TokenPath", "/token" },
            { "TokenQuery", "value=123" },
        } <<
        "web_server";
}

void OAuth2Test::testProcess()
{
    QFETCH(QString, clientId);
    QFETCH(QString, clientSecret);
    QFETCH(QStringList, scopes);
    QFETCH(QUrl, authorizationUrl);
    QFETCH(QUrl, accessTokenUrl);
    QFETCH(QString, responseType);
    QFETCH(QString, callbackUrl);
    QFETCH(int, protocolTweaks);
    QFETCH(QVariantMap, expectedSessionData);
    QFETCH(QString, expectedMechanism);

    MockPlugin *mockPlugin = new MockPlugin("oauth2");
    QSignalSpy processCalled(mockPlugin,
                             SIGNAL(processCalled(const QVariantMap &,
                                                  const QString &)));

    OAuth2 oauth2;

    oauth2.setClientId(clientId);
    oauth2.setClientSecret(clientSecret);
    oauth2.setScopes(scopes);
    oauth2.setAuthorizationUrl(authorizationUrl);
    oauth2.setAccessTokenUrl(accessTokenUrl);
    oauth2.setResponseType(responseType);
    oauth2.setCallbackUrl(callbackUrl);
    oauth2.setProtocolTweaks(OAuth2::ProtocolTweaks(protocolTweaks));

    oauth2.process();

    QCOMPARE(processCalled.count(), 1);
    QVariantMap sessionData = processCalled.at(0).at(0).toMap();
    QString mechanism = processCalled.at(0).at(1).toString();

    QCOMPARE(sessionData, expectedSessionData);
    QCOMPARE(mechanism, expectedMechanism);
}

void OAuth2Test::testUserActionRequired_data()
{
    QTest::addColumn<QVariantMap>("uiSessionData");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<QUrl>("expectedFinalUrl");
    QTest::addColumn<QUrl>("resultUrl");
    QTest::addColumn<QVariantMap>("expectedUiData");

    QTest::newRow("mostly empty") <<
        QVariantMap {
        } <<
        QUrl() <<
        QUrl() <<
        QUrl() <<
        QVariantMap {
            { "UrlResponse", "" },
        };

    QTest::newRow("rejected") <<
        QVariantMap {
            { "OpenUrl", "http://example.com/openme" },
            { "FinalUrl", "http://final.url/" },
        } <<
        QUrl("http://example.com/openme") <<
        QUrl("http://final.url/") <<
        QUrl("http://reject/") <<
        QVariantMap {
            { "QueryErrorCode", int(SignOn::QUERY_ERROR_CANCELED) },
        };

    QTest::newRow("accepted") <<
        QVariantMap {
            { "OpenUrl", "http://example.com/openme" },
            { "FinalUrl", "http://localhost:8000/" },
        } <<
        QUrl("http://example.com/openme") <<
        QUrl("http://localhost:8000/") <<
        QUrl("http://the.callback.url/path") <<
        QVariantMap {
            { "UrlResponse", "http://the.callback.url/path" },
        };
}

void OAuth2Test::testUserActionRequired()
{
    QFETCH(QVariantMap, uiSessionData);
    QFETCH(QUrl, expectedUrl);
    QFETCH(QUrl, expectedFinalUrl);
    QFETCH(QUrl, resultUrl);
    QFETCH(QVariantMap, expectedUiData);

    MockPlugin *mockPlugin = new MockPlugin("oauth2");
    QSignalSpy userActionFinishedCalled(mockPlugin,
                     SIGNAL(userActionFinishedCalled(const QVariantMap &)));

    OAuth2 oauth2;
    QSignalSpy actionRequested(&oauth2,
               SIGNAL(actionRequested(Authentication::ActionRequest)));

    oauth2.setClientId("client");
    oauth2.setClientSecret("secret");
    oauth2.setScopes({"friends"});
    oauth2.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth2.setAccessTokenUrl(QUrl("http://example.com/something/else"));
    oauth2.setResponseType("code");
    oauth2.setCallbackUrl("http://mysite.com/path");

    oauth2.process();

    mockPlugin->emitUserActionRequired(uiSessionData);

    QTRY_COMPARE(actionRequested.count(), 1);
    ActionRequest request =
        actionRequested.at(0).at(0).value<ActionRequest>();
    QVERIFY(request.isValid());
    QCOMPARE(request.url(), expectedUrl);
    QCOMPARE(request.finalUrl(), expectedFinalUrl);

    if (resultUrl.host() == "reject") {
        request.reject();
    } else {
        request.setResult(resultUrl);
    }

    QCOMPARE(userActionFinishedCalled.count(), 1);
    QVariantMap uiData = userActionFinishedCalled.at(0).at(0).toMap();
    QCOMPARE(uiData, expectedUiData);
}

void OAuth2Test::testError_data()
{
    QTest::addColumn<int>("errorCode");
    QTest::addColumn<int>("expectedErrorCode");

    QTest::newRow("unknown") <<
        int(SignOn::Error::Unknown) <<
        int(AbstractAuthenticator::InternalError);

    QTest::newRow("missing data") <<
        int(SignOn::Error::MissingData) <<
        int(AbstractAuthenticator::MissingData);

    QTest::newRow("not authorized") <<
        int(SignOn::Error::NotAuthorized) <<
        int(AbstractAuthenticator::NotAuthorized);

    QTest::newRow("invalid credentials") <<
        int(SignOn::Error::InvalidCredentials) <<
        int(AbstractAuthenticator::NotAuthorized);
}

void OAuth2Test::testError()
{
    QFETCH(int, errorCode);
    QFETCH(int, expectedErrorCode);

    MockPlugin *mockPlugin = new MockPlugin("oauth2");

    OAuth2 oauth2;
    QSignalSpy finished(&oauth2, SIGNAL(finished()));

    oauth2.setClientId("client");
    oauth2.setClientSecret("secret");
    oauth2.setScopes({"friends"});
    oauth2.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth2.setAccessTokenUrl(QUrl("http://example.com/something/else"));
    oauth2.setResponseType("code");
    oauth2.setCallbackUrl("http://mysite.com/path");

    oauth2.process();

    SignOn::Error error(errorCode, "Something happened");
    mockPlugin->emitError(error);

    QTRY_COMPARE(finished.count(), 1);
    QVERIFY(oauth2.hasError());

    QCOMPARE(oauth2.errorMessage(), QString("Something happened"));
    QCOMPARE(oauth2.errorCode(),
             AbstractAuthenticator::ErrorCode(expectedErrorCode));
}

void OAuth2Test::testResult()
{
    MockPlugin *mockPlugin = new MockPlugin("oauth2");

    OAuth2 oauth2;
    QSignalSpy finished(&oauth2, SIGNAL(finished()));

    oauth2.setClientId("client");
    oauth2.setClientSecret("secret");
    oauth2.setScopes({"friends"});
    oauth2.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth2.setAccessTokenUrl(QUrl("http://example.com/something/else"));
    oauth2.setResponseType("code");
    oauth2.setCallbackUrl("http://mysite.com/path");

    oauth2.process();

    QStringList scopes { "one", "two" };
    QVariantMap result {
        { "AccessToken", "aaaa" },
        { "RefreshToken", "bbbb" },
        { "ExpiresIn", 3600 },
        { "Scope", scopes },
        { "ExtraFields", QVariantMap {
               { "user_id", "john12" },
            }
        },
    };
    mockPlugin->emitResult(result);

    QTRY_COMPARE(finished.count(), 1);
    QVERIFY(!oauth2.hasError());

    QCOMPARE(oauth2.accessToken(), QByteArray("aaaa"));
    QCOMPARE(oauth2.refreshToken(), QByteArray("bbbb"));
    QCOMPARE(oauth2.expiresIn(), 3600);
    QCOMPARE(oauth2.grantedPermissions(), scopes);
    QVariantMap expectedFields {
        { "user_id", "john12" },
    };
    QCOMPARE(oauth2.extraFields(), expectedFields);
}

void OAuth2Test::testGet_data()
{
    QTest::addColumn<QUrl>("requestUrl");
    QTest::addColumn<int>("authorizationTransmission");
    QTest::addColumn<QVariantMap>("params");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<QString>("expectedHeader");

    QTest::newRow("default") <<
        QUrl("https://mysite.com") <<
        -1 <<
        QVariantMap {
            { "userId", "john23" },
            { "flag", true },
        } <<
        QUrl::fromUserInput("https://mysite.com?flag=true&userId=john23") <<
        "Bearer aaaa";

    QTest::newRow("default, query in url") <<
        QUrl("https://mysite.com?confirm=false") <<
        -1 <<
        QVariantMap {
            { "userId", "john23" },
            { "flag", true },
        } <<
        QUrl::fromUserInput("https://mysite.com?confirm=false&flag=true&userId=john23") <<
        "Bearer aaaa";

    QTest::newRow("in query") <<
        QUrl("https://mysite.com") <<
        int(OAuth2::ParametersInQuery) <<
        QVariantMap {
            { "userId", "john23" },
            { "flag", true },
        } <<
        QUrl::fromUserInput("https://mysite.com?flag=true&userId=john23&access_token=aaaa") <<
        "";

    QTest::newRow("in query, query in url") <<
        QUrl("https://mysite.com?force=yes") <<
        int(OAuth2::ParametersInQuery) <<
        QVariantMap {
            { "userId", "john23" },
            { "flag", true },
        } <<
        QUrl::fromUserInput("https://mysite.com?force=yes&flag=true&userId=john23&access_token=aaaa") <<
        "";
}

void OAuth2Test::testGet()
{
    QFETCH(QUrl, requestUrl);
    QFETCH(int, authorizationTransmission);
    QFETCH(QVariantMap, params);
    QFETCH(QUrl, expectedUrl);
    QFETCH(QString, expectedHeader);

    OAuth2 oauth2;

    FakeNam dummyNam;
    oauth2.setNetworkAccessManager(&dummyNam);
    oauth2.setClientId("client");
    oauth2.setClientSecret("secret");
    oauth2.injectToken("aaaa");

    if (authorizationTransmission >= 0) {
        OAuth2::ParameterTransmission transmission =
            static_cast<OAuth2::ParameterTransmission>(authorizationTransmission);
        oauth2.setAuthorizationTransmission(transmission);
    }
    QScopedPointer<QNetworkReply> reply(oauth2.get(requestUrl, params));
    QVERIFY(!reply.isNull());

    QNetworkRequest request = reply->request();
    QCOMPARE(request.url(), expectedUrl);
    QCOMPARE(request.rawHeader("Authorization"), expectedHeader.toUtf8());
}

void OAuth2Test::testGetRequest()
{
    FakeNamFactory *namFactory = new FakeNamFactory();
    QSignalSpy namCreated(namFactory, SIGNAL(namCreated(FakeNam*)));

    QQmlEngine engine;
    engine.setNetworkAccessManagerFactory(namFactory);
    qmlRegisterType<OAuth2>("MyTest", 1, 0, "OAuth2");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "OAuth2 {\n"
                      "  property string pageContents: \"\"\n"
                      "  onFinished: {\n"
                      "    var http = newGetRequest(\"https://example.com/path\", {\n"
                      "      'user': 'me',\n"
                      "      'visible': true,\n"
                      "    })\n"
                      "    http.onreadystatechange = function() {\n"
                      "      if (http.readyState === 4) {\n"
                      "        pageContents = http.responseText\n"
                      "      }\n"
                      "    }\n"
                      "    http.send()\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);

    QSignalSpy pageContentsChanged(object, SIGNAL(pageContentsChanged()));

    OAuth2 *oauth2 = qobject_cast<OAuth2*>(object);

    oauth2->setClientId("client");
    oauth2->setClientSecret("secret");
    oauth2->injectToken("aaaa");

    QTRY_COMPARE(namCreated.count(), 1);
    FakeNam *nam = namCreated.at(0).at(0).value<FakeNam*>();
    QVERIFY(nam);

    QTRY_COMPARE(pageContentsChanged.count(), 1);
    QByteArray contents = oauth2->property("pageContents").toByteArray();
    QJsonObject json = QJsonDocument::fromJson(contents).object();
    QCOMPARE(json.value("url").toString(),
             QString("https://example.com/path?user=me&visible=true"));

    QJsonObject headers = json.value("headers").toObject();
    QCOMPARE(headers.value("Authorization").toString(),
             QString("Bearer aaaa"));
}

void OAuth2Test::testLogout()
{
    OAuth2 oauth2;

    FakeNam dummyNam;
    oauth2.setNetworkAccessManager(&dummyNam);

    oauth2.setClientId("client");
    oauth2.setClientSecret("secret");
    oauth2.injectToken("aaaa");
    QCOMPARE(oauth2.accessToken(), QByteArray("aaaa"));

    oauth2.logout();
    QVERIFY(oauth2.accessToken().isEmpty());
    /* Authentication paramters, though, must still be set */
    QCOMPARE(oauth2.clientId(), QString("client"));
}

QTEST_GUILESS_MAIN(OAuth2Test)

#include "tst_oauth2.moc"
