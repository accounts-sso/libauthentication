/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef AUTHENTICATION_TEST_UTILS_H
#define AUTHENTICATION_TEST_UTILS_H

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTest>
#include <QVariantMap>

namespace QTest {

template<>
char *toString(const QVariantMap &p)
{
    QJsonDocument doc(QJsonObject::fromVariantMap(p));
    QByteArray ba = doc.toJson(QJsonDocument::Compact);
    return qstrdup(ba.data());
}

template<>
char *toString(const QHash<QByteArray,QByteArray> &p)
{
    QByteArray ba = "QHash(";
    for (auto i = p.begin(); i != p.end(); i++) {
        ba += '"';
        ba += i.key();
        ba += "\": ";
        ba += i.value();
        ba += ", ";
    }
    ba += ")";
    return qstrdup(ba.data());
}

} // QTest namespace

typedef QHash<QByteArray,QByteArray> Headers;

static inline Headers requestHeaders(const QNetworkRequest &req) {
    Headers headers;
    for (const QByteArray &header: req.rawHeaderList()) {
        headers.insert(header, req.rawHeader(header));
    }
    return headers;
}

#endif // AUTHENTICATION_TEST_UTILS_H
