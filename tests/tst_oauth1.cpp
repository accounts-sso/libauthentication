/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/AbstractAuthenticator"
#include "Authentication/ActionRequest"
#include "Authentication/OAuth1"
#include "Authentication/plugin_loader.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <SignOn/AuthPluginInterface>
#include "fake_network.h"
#include "test_utils.h"

using namespace Authentication;

/* Fake OAuth plugin */

class MockPlugin;
static MockPlugin *mockPlugin = 0;

class MockPlugin: public AuthPluginInterface
{
    Q_OBJECT
public:
    MockPlugin(const QString &baseName):
        m_type(baseName)
    {
        mockPlugin = this;
    }

    ~MockPlugin() { mockPlugin = 0; }

    QString type() const override { return m_type; }

    QStringList mechanisms() const override {
        return QStringList { "HMAC-SHA1", "PLAINTEXT" };
    }

    void process(const SignOn::SessionData &data,
                 const QString &mechanism) override {
        Q_EMIT processCalled(data.toMap(), mechanism);
    }

    void userActionFinished(const SignOn::UiSessionData &data) override {
        Q_EMIT userActionFinishedCalled(data.toMap());
    }

    void emitUserActionRequired(const QVariantMap &map) {
        Q_EMIT userActionRequired(SignOn::UiSessionData(map));
    }

    void emitResult(const QVariantMap &map) {
        Q_EMIT result(SignOn::SessionData(map));
    }

    void emitError(const SignOn::Error &err) {
        Q_EMIT error(err);
    }

Q_SIGNALS:
    void processCalled(const QVariantMap &map, const QString &mechanism);
    void userActionFinishedCalled(const QVariantMap &map);

private:
    QString m_type;
};

/* Mock plugin loader */

AuthPluginInterface *PluginLoader::load(const QString &baseName)
{
    return (mockPlugin && baseName == mockPlugin->type()) ? mockPlugin : 0;
}

/* Finally, the test class */

class OAuth1Test: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testGetters();
    void testNoPlugin();
    void testProcess_data();
    void testProcess();
    void testUserActionRequired_data();
    void testUserActionRequired();
    void testError_data();
    void testError();
    void testResult();
    void testGet_data();
    void testGet();
    void testGetRequest_data();
    void testGetRequest();
    void testPost_data();
    void testPost();
    void testSignedRequest_data();
    void testSignedRequest();
    void testLogout();
};

void OAuth1Test::testGetters()
{
    OAuth1 oauth1;

    oauth1.setClientId("aa 123");
    QCOMPARE(oauth1.clientId(), QString("aa 123"));

    oauth1.setClientSecret("&$%-as");
    QCOMPARE(oauth1.clientSecret(), QString("&$%-as"));

    QUrl tempUrl("https://example.com/credentials");
    oauth1.setTemporaryCredentialsUrl(tempUrl);
    QCOMPARE(oauth1.temporaryCredentialsUrl(), tempUrl);

    QUrl authUrl("https://example.com/authorize");
    oauth1.setAuthorizationUrl(authUrl);
    QCOMPARE(oauth1.authorizationUrl(), authUrl);

    QUrl tokenUrl("https://example.com/token");
    oauth1.setTokenRequestUrl(tokenUrl);
    QCOMPARE(oauth1.tokenRequestUrl(), tokenUrl);

    oauth1.setSignatureMethod("HMAC-SHA1");
    QCOMPARE(oauth1.signatureMethod(), QString("HMAC-SHA1"));

    oauth1.setRealm("Photos");
    QCOMPARE(oauth1.realm(), QString("Photos"));

    oauth1.setCallbackUrl("oob");
    QCOMPARE(oauth1.callbackUrl(), QString("oob"));

    QCOMPARE(oauth1.authorizationTransmission(), OAuth1::ParametersInQuery);
    oauth1.setAuthorizationTransmission(OAuth1::ParametersInBody);
    QCOMPARE(oauth1.authorizationTransmission(), OAuth1::ParametersInBody);
    oauth1.setAuthorizationTransmission(OAuth1::ParametersInHeader);
    QCOMPARE(oauth1.authorizationTransmission(), OAuth1::ParametersInHeader);
}

void OAuth1Test::testNoPlugin()
{
    OAuth1 oauth1;

    QSignalSpy finished(&oauth1, SIGNAL(finished()));
    oauth1.process();

    QCOMPARE(finished.count(), 1);
    QCOMPARE(oauth1.errorCode(), AbstractAuthenticator::InternalError);
    QCOMPARE(oauth1.errorMessage(), QString("Couldn't load OAuth plugin"));
}

void OAuth1Test::testProcess_data()
{
    QTest::addColumn<QString>("clientId");
    QTest::addColumn<QString>("clientSecret");
    QTest::addColumn<QUrl>("temporaryCredentialsUrl");
    QTest::addColumn<QUrl>("authorizationUrl");
    QTest::addColumn<QUrl>("tokenRequestUrl");
    QTest::addColumn<QString>("signatureMethod");
    QTest::addColumn<QString>("realm");
    QTest::addColumn<QString>("callbackUrl");
    QTest::addColumn<QString>("userAgent");
    QTest::addColumn<QVariantMap>("expectedSessionData");
    QTest::addColumn<QString>("expectedMechanism");

    QTest::newRow("mostly empty") <<
        "" <<
        "" <<
        QUrl() <<
        QUrl() <<
        QUrl() <<
        "" <<
        "" <<
        QString() <<
        QString() <<
        QVariantMap {
            { "AuthorizationEndpoint", "" },
            { "Callback", "" },
            { "ConsumerKey", "" },
            { "ConsumerSecret", "" },
            { "Realm", "" },
            { "RequestEndpoint", "" },
            { "TokenEndpoint", "" },
        } <<
        "";

    QTest::newRow("user_agent") <<
        "id123" <<
        "secret123" <<
        QUrl("https://example.com:3040/credentials") <<
        QUrl("https://example.com:8080/authorize") <<
        QUrl("https://example.com:2020/token") <<
        "PLAINTEXT" <<
        "user" <<
        "http://localhost/cb" <<
        "" <<
        QVariantMap {
            { "AuthorizationEndpoint", "https://example.com:8080/authorize" },
            { "Callback", "http://localhost/cb" },
            { "ConsumerKey", "id123" },
            { "ConsumerSecret", "secret123" },
            { "Realm", "user" },
            { "RequestEndpoint", "https://example.com:3040/credentials" },
            { "TokenEndpoint", "https://example.com:2020/token" },
        } <<
        "PLAINTEXT";

    QTest::newRow("web_server") <<
        "id123" <<
        "secret123" <<
        QUrl("https://example.com/get/credentials") <<
        QUrl("https://example.com/authorize") <<
        QUrl("https://other.com/token") <<
        "HMAC-SHA1" <<
        QString() <<
        "oob" <<
        "" <<
        QVariantMap {
            { "AuthorizationEndpoint", "https://example.com/authorize" },
            { "Callback", "oob" },
            { "ConsumerKey", "id123" },
            { "ConsumerSecret", "secret123" },
            { "Realm", "" },
            { "RequestEndpoint", "https://example.com/get/credentials" },
            { "TokenEndpoint", "https://other.com/token" },
        } <<
        "HMAC-SHA1";

    QTest::newRow("User-Agent header") <<
        "" <<
        "" <<
        QUrl() <<
        QUrl() <<
        QUrl() <<
        "" <<
        "" <<
        QString() <<
        "MyApplication/1.0" <<
        QVariantMap {
            { "AuthorizationEndpoint", "" },
            { "Callback", "" },
            { "ConsumerKey", "" },
            { "ConsumerSecret", "" },
            { "Realm", "" },
            { "RequestEndpoint", "" },
            { "TokenEndpoint", "" },
            { "UserAgent", "MyApplication/1.0" },
        } <<
        "";
}

void OAuth1Test::testProcess()
{
    QFETCH(QString, clientId);
    QFETCH(QString, clientSecret);
    QFETCH(QUrl, temporaryCredentialsUrl);
    QFETCH(QUrl, authorizationUrl);
    QFETCH(QUrl, tokenRequestUrl);
    QFETCH(QString, signatureMethod);
    QFETCH(QString, realm);
    QFETCH(QString, callbackUrl);
    QFETCH(QString, userAgent);
    QFETCH(QVariantMap, expectedSessionData);
    QFETCH(QString, expectedMechanism);

    MockPlugin *mockPlugin = new MockPlugin("oauth2");
    QSignalSpy processCalled(mockPlugin,
                             SIGNAL(processCalled(const QVariantMap &,
                                                  const QString &)));

    OAuth1 oauth1;

    oauth1.setClientId(clientId);
    oauth1.setClientSecret(clientSecret);
    oauth1.setTemporaryCredentialsUrl(temporaryCredentialsUrl);
    oauth1.setAuthorizationUrl(authorizationUrl);
    oauth1.setTokenRequestUrl(tokenRequestUrl);
    oauth1.setSignatureMethod(signatureMethod);
    oauth1.setRealm(realm);
    oauth1.setCallbackUrl(callbackUrl);
    oauth1.setUserAgent(userAgent);

    oauth1.process();

    QCOMPARE(processCalled.count(), 1);
    QVariantMap sessionData = processCalled.at(0).at(0).toMap();
    QString mechanism = processCalled.at(0).at(1).toString();

    QCOMPARE(sessionData, expectedSessionData);
    QCOMPARE(mechanism, expectedMechanism);
}

void OAuth1Test::testUserActionRequired_data()
{
    QTest::addColumn<QVariantMap>("uiSessionData");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<QUrl>("expectedFinalUrl");
    QTest::addColumn<QUrl>("resultUrl");
    QTest::addColumn<QVariantMap>("expectedUiData");

    QTest::newRow("mostly empty") <<
        QVariantMap {
        } <<
        QUrl() <<
        QUrl() <<
        QUrl() <<
        QVariantMap {
            { "UrlResponse", "" },
        };

    QTest::newRow("rejected") <<
        QVariantMap {
            { "OpenUrl", "http://example.com/openme" },
            { "FinalUrl", "http://final.url/" },
        } <<
        QUrl("http://example.com/openme") <<
        QUrl("http://final.url/") <<
        QUrl("http://reject/") <<
        QVariantMap {
            { "QueryErrorCode", int(SignOn::QUERY_ERROR_CANCELED) },
        };

    QTest::newRow("accepted") <<
        QVariantMap {
            { "OpenUrl", "http://example.com/openme" },
            { "FinalUrl", "http://localhost:8000/" },
        } <<
        QUrl("http://example.com/openme") <<
        QUrl("http://localhost:8000/") <<
        QUrl("http://the.callback.url/path") <<
        QVariantMap {
            { "UrlResponse", "http://the.callback.url/path" },
        };
}

void OAuth1Test::testUserActionRequired()
{
    QFETCH(QVariantMap, uiSessionData);
    QFETCH(QUrl, expectedUrl);
    QFETCH(QUrl, expectedFinalUrl);
    QFETCH(QUrl, resultUrl);
    QFETCH(QVariantMap, expectedUiData);

    MockPlugin *mockPlugin = new MockPlugin("oauth2");
    QSignalSpy userActionFinishedCalled(mockPlugin,
                     SIGNAL(userActionFinishedCalled(const QVariantMap &)));

    OAuth1 oauth1;
    QSignalSpy actionRequested(&oauth1,
               SIGNAL(actionRequested(Authentication::ActionRequest)));

    oauth1.setClientId("client");
    oauth1.setClientSecret("secret");
    oauth1.setTemporaryCredentialsUrl(QUrl("http://example.com/one.thing"));
    oauth1.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth1.setTokenRequestUrl(QUrl("http://example.com/something/else"));
    oauth1.setSignatureMethod("HMAC-SHA1");
    oauth1.setRealm("albums");
    oauth1.setCallbackUrl("http://mysite.com/path");

    oauth1.process();

    mockPlugin->emitUserActionRequired(uiSessionData);

    QTRY_COMPARE(actionRequested.count(), 1);
    ActionRequest request =
        actionRequested.at(0).at(0).value<ActionRequest>();
    QVERIFY(request.isValid());
    QCOMPARE(request.url(), expectedUrl);
    QCOMPARE(request.finalUrl(), expectedFinalUrl);

    if (resultUrl.host() == "reject") {
        request.reject();
    } else {
        request.setResult(resultUrl);
    }

    QCOMPARE(userActionFinishedCalled.count(), 1);
    QVariantMap uiData = userActionFinishedCalled.at(0).at(0).toMap();
    QCOMPARE(uiData, expectedUiData);
}

void OAuth1Test::testError_data()
{
    QTest::addColumn<int>("errorCode");
    QTest::addColumn<int>("expectedErrorCode");

    QTest::newRow("unknown") <<
        int(SignOn::Error::Unknown) <<
        int(AbstractAuthenticator::InternalError);

    QTest::newRow("missing data") <<
        int(SignOn::Error::MissingData) <<
        int(AbstractAuthenticator::MissingData);

    QTest::newRow("not authorized") <<
        int(SignOn::Error::NotAuthorized) <<
        int(AbstractAuthenticator::NotAuthorized);

    QTest::newRow("invalid credentials") <<
        int(SignOn::Error::InvalidCredentials) <<
        int(AbstractAuthenticator::NotAuthorized);

    QTest::newRow("permission denied") <<
        int(SignOn::Error::PermissionDenied) <<
        int(AbstractAuthenticator::NotAuthorized);

    QTest::newRow("operation failed") <<
        int(SignOn::Error::OperationFailed) <<
        int(AbstractAuthenticator::ServerError);
}

void OAuth1Test::testError()
{
    QFETCH(int, errorCode);
    QFETCH(int, expectedErrorCode);

    MockPlugin *mockPlugin = new MockPlugin("oauth2");

    OAuth1 oauth1;
    QSignalSpy finished(&oauth1, SIGNAL(finished()));

    oauth1.setClientId("client");
    oauth1.setClientSecret("secret");
    oauth1.setTemporaryCredentialsUrl(QUrl("http://example.com/one.thing"));
    oauth1.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth1.setTokenRequestUrl(QUrl("http://example.com/something/else"));
    oauth1.setSignatureMethod("HMAC-SHA1");
    oauth1.setRealm("albums");
    oauth1.setCallbackUrl("http://mysite.com/path");

    oauth1.process();

    SignOn::Error error(errorCode, "Something happened");
    mockPlugin->emitError(error);

    QTRY_COMPARE(finished.count(), 1);
    QVERIFY(oauth1.hasError());

    QCOMPARE(oauth1.errorMessage(), QString("Something happened"));
    QCOMPARE(oauth1.errorCode(),
             AbstractAuthenticator::ErrorCode(expectedErrorCode));
}

void OAuth1Test::testResult()
{
    MockPlugin *mockPlugin = new MockPlugin("oauth2");

    OAuth1 oauth1;
    QSignalSpy finished(&oauth1, SIGNAL(finished()));

    oauth1.setClientId("client");
    oauth1.setClientSecret("secret");
    oauth1.setTemporaryCredentialsUrl(QUrl("http://example.com/one.thing"));
    oauth1.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth1.setTokenRequestUrl(QUrl("http://example.com/something/else"));
    oauth1.setSignatureMethod("HMAC-SHA1");
    oauth1.setRealm("albums");
    oauth1.setCallbackUrl("http://mysite.com/path");

    oauth1.process();

    QVariantMap result {
        { "AccessToken", "aaaa" },
        { "TokenSecret", "bbbb" },
    };
    mockPlugin->emitResult(result);

    QTRY_COMPARE(finished.count(), 1);
    QVERIFY(!oauth1.hasError());

    QCOMPARE(oauth1.token(), QByteArray("aaaa"));
    QCOMPARE(oauth1.tokenSecret(), QByteArray("bbbb"));
}

void OAuth1Test::testGet_data()
{
    QTest::addColumn<QUrl>("requestUrl");
    QTest::addColumn<int>("authorizationTransmission");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<QString>("expectedHeader");
    QTest::addColumn<QStringList>("expectedWarnings");

    QTest::newRow("default") <<
        QUrl("https://mysite.com") <<
        int(OAuth1::ParametersInQuery) <<
        QUrl("https://mysite.com?flag=true&userId=john23"
             "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
             "&oauth_consumer_key=client&oauth_token=aaaa"
             "&oauth_timestamp=1420534672&oauth_nonce=32481369"
             "&oauth_signature=MXJHK7GKKhHnpq452IedOPy%2Fkac%3D") <<
        "" <<
        QStringList {};

    QTest::newRow("default, query in url") <<
        QUrl("https://mysite.com?new=no") <<
        int(OAuth1::ParametersInQuery) <<
        QUrl("https://mysite.com?new=no&flag=true&userId=john23"
             "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
             "&oauth_consumer_key=client&oauth_token=aaaa"
             "&oauth_timestamp=1420534672&oauth_nonce=32481369"
             "&oauth_signature=912fOyc9%2BieoNs6jbgxCGkdl04s%3D") <<
        "" <<
        QStringList {};

    QTest::newRow("in query, query in url") <<
        QUrl("https://mysite.com?param=34") <<
        int(OAuth1::ParametersInQuery) <<
        QUrl("https://mysite.com?param=34&flag=true&userId=john23"
             "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
             "&oauth_consumer_key=client&oauth_token=aaaa"
             "&oauth_timestamp=1420534672&oauth_nonce=32481369"
             "&oauth_signature=emDfsqoHShVP45Rh7E3OyfGFlWs%3D") <<
        "" <<
        QStringList {};

    QTest::newRow("in header") <<
        QUrl("https://mysite.com") <<
        int(OAuth1::ParametersInHeader) <<
        QUrl("https://mysite.com?flag=true&userId=john23") <<
        "OAuth oauth_signature_method=\"HMAC-SHA1\", oauth_version=\"1.0\", "
        "oauth_consumer_key=\"client\", oauth_token=\"aaaa\", "
        "oauth_timestamp=\"1420534672\", oauth_nonce=\"32481369\", "
        "oauth_signature=\"MXJHK7GKKhHnpq452IedOPy%2Fkac%3D\"" <<
        QStringList {};

    QTest::newRow("in header, query in url") <<
        QUrl("https://mysite.com?one=1&two=2") <<
        int(OAuth1::ParametersInHeader) <<
        QUrl("https://mysite.com?one=1&two=2&flag=true&userId=john23") <<
        "OAuth oauth_signature_method=\"HMAC-SHA1\", oauth_version=\"1.0\", "
        "oauth_consumer_key=\"client\", oauth_token=\"aaaa\", "
        "oauth_timestamp=\"1420534672\", oauth_nonce=\"32481369\", "
        "oauth_signature=\"a2j%2BkIQxpXwJhcbkkMfHZ2urFjU%3D\"" <<
        QStringList {};

    QTest::newRow("authorization in body") <<
        QUrl("https://mysite.com?param=34") <<
        int(OAuth1::ParametersInBody) <<
        QUrl() <<
        "" <<
        QStringList { "Can't send OAuth params in body (GET request)!" };

    QTest::newRow("wrong parameters flag") <<
        QUrl("https://mysite.com?param=34") <<
        int(23412) <<
        QUrl() <<
        "" <<
        QStringList {
            "Invalid OAuth params transmission: "
            "Authentication::AbstractAuthenticator::ParameterTransmission(23412)"
        };
}

void OAuth1Test::testGet()
{
    QFETCH(QUrl, requestUrl);
    QFETCH(int, authorizationTransmission);
    QFETCH(QUrl, expectedUrl);
    QFETCH(QString, expectedHeader);
    QFETCH(QStringList, expectedWarnings);

    for (const QString &msg: expectedWarnings) {
        QTest::ignoreMessage(QtWarningMsg, msg.toUtf8().constData());
    }

    OAuth1 oauth1;

    FakeNam dummyNam;
    oauth1.setNetworkAccessManager(&dummyNam);

    oauth1.setClientId("client");
    oauth1.setClientSecret("secret");
    oauth1.setSignatureMethod("HMAC-SHA1");
    if (authorizationTransmission >= 0) {
        OAuth1::ParameterTransmission transmission =
            static_cast<OAuth1::ParameterTransmission>(authorizationTransmission);
        oauth1.setAuthorizationTransmission(transmission);
    }
    oauth1.injectTokens("aaaa", "bbbb");

    QScopedPointer<QNetworkReply> reply(oauth1.get(requestUrl, {
        { "userId", "john23" },
        { "flag", true },
    }));
    if (expectedWarnings.isEmpty()) {
        QVERIFY(!reply.isNull());

        QNetworkRequest request = reply->request();
        QCOMPARE(request.url(), expectedUrl);
        QCOMPARE(request.rawHeader("Authorization"), expectedHeader.toLatin1());
    } else {
        QVERIFY(reply.isNull());
    }
}

void OAuth1Test::testGetRequest_data()
{
    QTest::addColumn<QUrl>("requestUrl");
    QTest::addColumn<int>("authorizationTransmission");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<QString>("expectedHeader");

    QTest::newRow("default") <<
        QUrl("https://mysite.com") <<
        int(OAuth1::ParametersInQuery) <<
        QUrl("https://mysite.com?user=me&visible=true"
             "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
             "&oauth_consumer_key=client&oauth_token=aaaa"
             "&oauth_timestamp=1420534672&oauth_nonce=32481369"
             "&oauth_signature=aXTiDnP6phIjGPbmors6oBivE9I%3D") <<
        "";

    QTest::newRow("default, query in url") <<
        QUrl("https://mysite.com?new=no") <<
        int(OAuth1::ParametersInQuery) <<
        QUrl("https://mysite.com?new=no&user=me&visible=true"
             "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
             "&oauth_consumer_key=client&oauth_token=aaaa"
             "&oauth_timestamp=1420534672&oauth_nonce=32481369"
             "&oauth_signature=DOklDsFrXXhAEW1ClyhZWAQT55E%3D") <<
        "";

    QTest::newRow("in query, query in url") <<
        QUrl("https://mysite.com?param=34") <<
        int(OAuth1::ParametersInQuery) <<
        QUrl("https://mysite.com?param=34&user=me&visible=true"
             "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
             "&oauth_consumer_key=client&oauth_token=aaaa"
             "&oauth_timestamp=1420534672&oauth_nonce=32481369"
             "&oauth_signature=OTbd%2BQ%2BEP1qG5%2BOjrdKwGMwY3VY%3D") <<
        "";

    QTest::newRow("in header") <<
        QUrl("https://mysite.com") <<
        int(OAuth1::ParametersInHeader) <<
        QUrl("https://mysite.com?user=me&visible=true") <<
        "OAuth oauth_signature_method=\"HMAC-SHA1\", oauth_version=\"1.0\", "
        "oauth_consumer_key=\"client\", oauth_token=\"aaaa\", "
        "oauth_timestamp=\"1420534672\", oauth_nonce=\"32481369\", "
        "oauth_signature=\"aXTiDnP6phIjGPbmors6oBivE9I%3D\"";

    QTest::newRow("in header, query in url") <<
        QUrl("https://mysite.com?one=1&two=2") <<
        int(OAuth1::ParametersInHeader) <<
        QUrl("https://mysite.com?one=1&two=2&user=me&visible=true") <<
        "OAuth oauth_signature_method=\"HMAC-SHA1\", oauth_version=\"1.0\", "
        "oauth_consumer_key=\"client\", oauth_token=\"aaaa\", "
        "oauth_timestamp=\"1420534672\", oauth_nonce=\"32481369\", "
        "oauth_signature=\"Ma0mL568pa5sRUKdC9uhr1RaTXg%3D\"";
}

void OAuth1Test::testGetRequest()
{
    QFETCH(QUrl, requestUrl);
    QFETCH(int, authorizationTransmission);
    QFETCH(QUrl, expectedUrl);
    QFETCH(QString, expectedHeader);

    FakeNamFactory *namFactory = new FakeNamFactory();
    QSignalSpy namCreated(namFactory, SIGNAL(namCreated(FakeNam*)));

    QQmlEngine engine;
    engine.setNetworkAccessManagerFactory(namFactory);
    qmlRegisterType<OAuth1>("MyTest", 1, 0, "OAuth1");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "OAuth1 {\n"
                      "  property string pageContents: \"\"\n"
                      "  property url requestUrl\n"
                      "  onFinished: {\n"
                      "    var http = newGetRequest(requestUrl, {\n"
                      "      'user': 'me',\n"
                      "      'visible': true,\n"
                      "    })\n"
                      "    http.onreadystatechange = function() {\n"
                      "      if (http.readyState === 4) {\n"
                      "        pageContents = http.responseText\n"
                      "      }\n"
                      "    }\n"
                      "    http.send()\n"
                      "  }\n"
                      "}",
                      QUrl());
    QObject *object = component.create();
    QVERIFY(object != 0);
    object->setProperty("requestUrl", requestUrl);

    QSignalSpy pageContentsChanged(object, SIGNAL(pageContentsChanged()));

    OAuth1 *oauth1 = qobject_cast<OAuth1*>(object);

    oauth1->setClientId("client");
    oauth1->setClientSecret("secret");
    oauth1->setSignatureMethod("HMAC-SHA1");
    oauth1->setRealm("albums");
    if (authorizationTransmission >= 0) {
        OAuth1::ParameterTransmission transmission =
            static_cast<OAuth1::ParameterTransmission>(authorizationTransmission);
        oauth1->setAuthorizationTransmission(transmission);
    }
    oauth1->injectTokens("aaaa", QByteArray());

    QTRY_COMPARE(namCreated.count(), 1);
    FakeNam *nam = namCreated.at(0).at(0).value<FakeNam*>();
    QVERIFY(nam);

    QTRY_COMPARE(pageContentsChanged.count(), 1);
    QByteArray contents = oauth1->property("pageContents").toByteArray();
    QJsonObject json = QJsonDocument::fromJson(contents).object();
    QCOMPARE(json.value("url").toString(), expectedUrl.toString());

    QJsonObject headers = json.value("headers").toObject();
    QCOMPARE(headers.value("Authorization").toString(),
             expectedHeader);
}

void OAuth1Test::testPost_data()
{
    QTest::addColumn<QUrl>("requestUrl");
    QTest::addColumn<QVariantMap>("requestParams");
    QTest::addColumn<int>("authorizationTransmission");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<Headers>("expectedHeaders");
    QTest::addColumn<QString>("expectedBody");

    QTest::newRow("params in body") <<
        QUrl("https://mysite.com") <<
        QVariantMap {
            { "userId", "timmy" },
            { "flag", false },
        } <<
        int(OAuth1::ParametersInBody) <<
        QUrl("https://mysite.com") <<
        Headers {
            { "Content-Type", "application/x-www-form-urlencoded" },
        } <<
        "flag=false&userId=timmy"
        "&oauth_signature_method=HMAC-SHA1&oauth_version=1.0"
        "&oauth_consumer_key=client&oauth_token=aaaa"
        "&oauth_timestamp=1420534672&oauth_nonce=32481369"
        "&oauth_signature=9%252Bxz3BT27U6ASgiwqO3BCN8oqHM%253D";

    QTest::newRow("multipart") <<
        QUrl("https://mysite.com") <<
        QVariantMap {
            { "userId", "timmy" },
            { "number", 12 },
            { "doc", QVariantMap {
                    { "contentType", "text/plain" },
                    { "filePath", TEST_FILE },
                    { "fileName", "My Story.txt" },
                }
            },
        } <<
        int(OAuth1::ParametersInBody) <<
        QUrl("https://mysite.com") <<
        Headers {
            { "MIME-Version", "1.0" },
            { "Content-Type", "multipart/form-data; boundary=\"BOUNDARY\";charset=UTF-8" },
        } <<
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"doc\"; filename=\"My Story.txt\"\r\n"
        "Content-Type: text/plain\r\n"
        "\r\n"
        "Story\n=====\n\nThis is a very sad story. It begins here,\n"
        "and it ends here.\n\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"number\"\r\n"
        "\r\n"
        "12\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"userId\"\r\n"
        "\r\n"
        "timmy\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_signature_method\"\r\n"
        "\r\n"
        "HMAC-SHA1\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_version\"\r\n"
        "\r\n"
        "1.0\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_consumer_key\"\r\n"
        "\r\n"
        "client\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_token\"\r\n"
        "\r\n"
        "aaaa\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_timestamp\"\r\n"
        "\r\n"
        "1420534672\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_nonce\"\r\n"
        "\r\n"
        "32481369\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_signature\"\r\n"
        "\r\n"
        "sJLTESAEGF0YkdEJAHR0rXzu9FI=\r\n"
        "--BOUNDARY--\r\n";

    // Regression test for a specific post
    QTest::newRow("PhotoTeleport+Flickr+newline") <<
        QUrl("https://up.flickr.com/services/upload/") <<
        QVariantMap {
            { "description", QStringLiteral("Model: Милана\nHelsinki, 2018") },
            { "photo", QVariantMap {
                    { "contentType", "image/jpeg" },
                    { "fileName", "me.jpg" },
                    { "filePath", TEST_FILE },
                }
            },
        } <<
        int(OAuth1::ParametersInBody) <<
        QUrl("https://up.flickr.com/services/upload/") <<
        Headers {
            { "MIME-Version", "1.0" },
            { "Content-Type", "multipart/form-data; boundary=\"BOUNDARY\";charset=UTF-8" },
        } <<
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"photo\"; filename=\"me.jpg\"\r\n"
        "Content-Type: image/jpeg\r\n"
        "\r\n"
        "Story\n=====\n\nThis is a very sad story. It begins here,\n"
        "and it ends here.\n\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"description\"\r\n"
        "\r\n"
        "Model: %D0%9C%D0%B8%D0%BB%D0%B0%D0%BD%D0%B0\nHelsinki, 2018\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_signature_method\"\r\n"
        "\r\n"
        "HMAC-SHA1\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_version\"\r\n"
        "\r\n"
        "1.0\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_consumer_key\"\r\n"
        "\r\n"
        "client\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_token\"\r\n"
        "\r\n"
        "aaaa\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_timestamp\"\r\n"
        "\r\n"
        "1420534672\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_nonce\"\r\n"
        "\r\n"
        "32481369\r\n"
        "--BOUNDARY\r\n"
        "Content-Disposition: form-data; name=\"oauth_signature\"\r\n"
        "\r\n"
        "DfXRwugN8K65hB/O2tq4XJhwHT0=\r\n"
        "--BOUNDARY--\r\n";
}

void OAuth1Test::testPost()
{
    QFETCH(QUrl, requestUrl);
    QFETCH(QVariantMap, requestParams);
    QFETCH(int, authorizationTransmission);
    QFETCH(QUrl, expectedUrl);
    QFETCH(Headers, expectedHeaders);
    QFETCH(QString, expectedBody);

    OAuth1 oauth1;
    QSignalSpy finished(&oauth1, SIGNAL(finished()));

    FakeNam dummyNam;
    oauth1.setNetworkAccessManager(&dummyNam);

    oauth1.setClientId("client");
    oauth1.setClientSecret("secret");
    oauth1.setSignatureMethod("HMAC-SHA1");
    oauth1.setRealm("albums");
    if (authorizationTransmission >= 0) {
        OAuth1::ParameterTransmission transmission =
            static_cast<OAuth1::ParameterTransmission>(authorizationTransmission);
        oauth1.setAuthorizationTransmission(transmission);
    }
    oauth1.injectTokens("aaaa", "bbbb");

    QScopedPointer<QNetworkReply> reply(oauth1.post(requestUrl,
                                                    requestParams));
    QVERIFY(!reply.isNull());

    QJsonObject json = QJsonDocument::fromJson(reply->readAll()).object();
    QString body = json.value("body").toString();
    QRegularExpression re(R"(--(boundary_\.oOo\.[^\r]*))");
    QRegularExpressionMatch match = re.match(body);
    if (match.hasMatch()) {
        QString boundary = match.captured(1);
        expectedBody.replace("BOUNDARY", boundary);
        for (auto i = expectedHeaders.begin();
             i != expectedHeaders.end();
             i++) {
            i.value().replace("BOUNDARY", boundary.toUtf8());
        }
    }

    QNetworkRequest request = reply->request();
    QCOMPARE(request.url(), expectedUrl);
    QCOMPARE(requestHeaders(request), expectedHeaders);

    /* Given that the body can be lengthy and QCOMPARE would only print the
     * first 100 characters or so, we split the strings in chunks can compare
     * them chunk by chunk; in this way, it will be easier to spot the
     * difference, in case the comparison fails. */
    for (int i = 0; i < qMax(expectedBody.count(), body.count()); i += 100) {
        QCOMPARE(body.mid(i, 100), expectedBody.mid(i, 100));
    }
}

void OAuth1Test::testSignedRequest_data()
{
    /* authData is made of:
     * client ID, client secret, access token, token secret, nonce, timestamp
     */
    QTest::addColumn<QStringList>("authData");
    QTest::addColumn<QString>("verb");
    QTest::addColumn<QUrl>("url");
    QTest::addColumn<QVariantMap>("parameters");
    QTest::addColumn<QUrl>("expectedUrl");
    QTest::addColumn<QString>("expectedAuthHeader");

    // from https://oauth.net/core/1.0a
    QTest::newRow("oauth doc example") <<
        QStringList {
            "dpf43f3p2l4k3l03", "kd94hf93k423kf44",
            "nnch734d00sl2jdk", "pfkkdhi9sl3r4s00",
            "kllo9940pd9333jh", "1191242096",
        } <<
        "GET" <<
        QUrl("http://photos.example.net/photos") <<
        QVariantMap {
            { "file", "vacation.jpg" },
            { "size", "original" },
        } <<
        QUrl("http://photos.example.net/photos?file=vacation.jpg&size=original") <<
        "OAuth"
        " oauth_signature_method=\"HMAC-SHA1\","
        " oauth_version=\"1.0\","
        " oauth_consumer_key=\"dpf43f3p2l4k3l03\","
        " oauth_token=\"nnch734d00sl2jdk\","
        " oauth_timestamp=\"1191242096\","
        " oauth_nonce=\"kllo9940pd9333jh\","
        " oauth_signature=\""
        + QString::fromLatin1(QUrl::toPercentEncoding("tR3+Ty81lMeYAr/Fid0kMTYa/WM="))
        + "\"";

    // from https://dev.twitter.com/oauth/overview/creating-signatures
    QTest::newRow("twitter doc example") <<
        QStringList {
            "xvz1evFS4wEEPTGEFPHBog", "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw",
            "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
            "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE",
            "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg", "1318622958",
        } <<
        "POST" <<
        QUrl("https://api.twitter.com/1/statuses/update.json") <<
        QVariantMap {
            { "include_entities", true },
            { "status", "Hello Ladies + Gentlemen, a signed OAuth request!" },
        } <<
        QUrl("https://api.twitter.com/1/statuses/update.json"
             "?include_entities=true"
             "&status=Hello%20Ladies%20+%20Gentlemen,"
             "%20a%20signed%20OAuth%20request!") <<
        "OAuth"
        " oauth_signature_method=\"HMAC-SHA1\","
        " oauth_version=\"1.0\","
        " oauth_consumer_key=\"xvz1evFS4wEEPTGEFPHBog\","
        " oauth_token=\"370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb\","
        " oauth_timestamp=\"1318622958\","
        " oauth_nonce=\"kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg\","
        " oauth_signature=\""
        + QString::fromLatin1(QUrl::toPercentEncoding("tnnArxj06cWHq44gCs1OSKk/jLY="))
        + "\"";
}

void OAuth1Test::testSignedRequest()
{
    QFETCH(QStringList, authData);
    QFETCH(QString, verb);
    QFETCH(QUrl, url);
    QFETCH(QVariantMap, parameters);
    QFETCH(QUrl, expectedUrl);
    QFETCH(QString, expectedAuthHeader);

    OAuth1 oauth1;
    oauth1.setClientId(authData[0]);
    oauth1.setClientSecret(authData[1]);
    oauth1.setTemporaryCredentialsUrl(QUrl("http://example.com/one.thing"));
    oauth1.setAuthorizationUrl(QUrl("http://example.com/something"));
    oauth1.setTokenRequestUrl(QUrl("http://example.com/something/else"));
    oauth1.setSignatureMethod("HMAC-SHA1");
    oauth1.setRealm("albums");
    oauth1.setCallbackUrl("http://mysite.com/path");
    oauth1.injectTokens(authData[2].toUtf8(), authData[3].toUtf8());
    qputenv("LIBAUTHENTICATION_TEST_oauth_nonce", authData[4].toUtf8());
    qputenv("LIBAUTHENTICATION_TEST_oauth_timestamp", authData[5].toUtf8());


    QNetworkRequest request =
        oauth1.authorizedRequest(verb.toUtf8(), url, parameters);
    QCOMPARE(request.url(), expectedUrl);

    QByteArray header = request.rawHeader("Authorization");
    QByteArray expectedHeader = expectedAuthHeader.toUtf8();
    /* Given that the body can be lengthy and QCOMPARE would only print the
     * first 100 characters or so, we split the strings in chunks can compare
     * them chunk by chunk; in this way, it will be easier to spot the
     * difference, in case the comparison fails. */
    for (int i = 0; i < qMax(expectedHeader.count(), header.count()); i += 100) {
        QCOMPARE(header.mid(i, 100), expectedHeader.mid(i, 100));
    }
    // Just to be safe
    QCOMPARE(header, expectedHeader);
}

void OAuth1Test::testLogout()
{
    OAuth1 oauth1;

    FakeNam dummyNam;
    oauth1.setNetworkAccessManager(&dummyNam);

    oauth1.setClientId("client");
    oauth1.setClientSecret("secret");
    oauth1.setSignatureMethod("HMAC-SHA1");
    oauth1.injectTokens("aaaa", "bbbb");
    QCOMPARE(oauth1.token(), QByteArray("aaaa"));

    oauth1.logout();
    QVERIFY(oauth1.token().isEmpty());
    QVERIFY(oauth1.tokenSecret().isEmpty());
    /* Authentication paramters, though, must still be set */
    QCOMPARE(oauth1.clientId(), QString("client"));
}

QTEST_GUILESS_MAIN(OAuth1Test)

#include "tst_oauth1.moc"
