/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/plugin_loader.h"
#include "SignOn/AuthPluginInterface"
#include "SignOn/PluginLoader"

#include <QDebug>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Authentication;

/* Mock SignOn plugin loader */

namespace SignOn {

class PluginLoaderPrivate: public QObject {
    Q_OBJECT

public:
    PluginLoaderPrivate(): m_loadReply(nullptr) {
        m_instance = this;
    }

    static PluginLoaderPrivate *instance() { return m_instance; }

    void setLoadReply(AuthPluginInterface *interface) {
        m_loadReply = interface;
    }

Q_SIGNALS:
    void loadCalled(const QString &baseName);

private:
    friend class PluginLoader;
    static PluginLoaderPrivate *m_instance;
    AuthPluginInterface *m_loadReply;
};

PluginLoaderPrivate *PluginLoaderPrivate::m_instance = nullptr;

PluginLoader::PluginLoader()
{
    d_ptr.reset(PluginLoaderPrivate::instance());
}

PluginLoader::~PluginLoader()
{
    /* Avoid destroying PluginLoaderPrivate, because that's our mock class */
    d_ptr.take();
}

AuthPluginInterface *PluginLoader::load(const QString &baseName) const
{
    PluginLoaderPrivate *d = const_cast<PluginLoaderPrivate*>(d_ptr.data());
    Q_EMIT d->loadCalled(baseName);
    return d->m_loadReply;
}

} // namespace SignOn

using MockPluginLoader = SignOn::PluginLoaderPrivate;

/* Finally, the test class */

class PluginLoaderTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testLoad();
};

void PluginLoaderTest::testLoad()
{
    MockPluginLoader mock;
    QSignalSpy loadCalled(&mock, &MockPluginLoader::loadCalled);

    AuthPluginInterface *expectedInterface =
        reinterpret_cast<AuthPluginInterface*>(0x12345678);
    mock.setLoadReply(expectedInterface);

    AuthPluginInterface *interface = PluginLoader::load("something");
    QCOMPARE(loadCalled.count(), 1);
    QCOMPARE(loadCalled.at(0).at(0).toString(), QString("something"));

    QCOMPARE(interface, expectedInterface);
}

QTEST_GUILESS_MAIN(PluginLoaderTest)

#include "tst_plugin_loader.moc"
