/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <QDebug>
#include <QQmlEngine>
#include <QTcpServer>
#include <QTcpSocket>
#include <QtQml>
#include <QtQuickTest>

class SocketTester: public QObject
{
    Q_OBJECT

public Q_SLOTS:
    bool connectToPort(int port) {
        m_socket.connectToHost("localhost", uint16_t(port));
        return m_socket.waitForConnected(2000);
    }

    bool disconnect() {
        m_socket.disconnectFromHost();
        return m_socket.waitForDisconnected(2000);
    }

    QObject *occupyPort(int port) {
        QTcpServer *server = new QTcpServer(this);
        bool ok = server->listen(QHostAddress::Any, uint16_t(port));
        if (!ok) {
            delete server;
            server = nullptr;
        }
        return server;
    }

private:
    QTcpSocket m_socket;
};

class Setup: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void qmlEngineAvailable(QQmlEngine *engine) {
        Q_UNUSED(engine);
        qmlRegisterType<SocketTester>("TestSupport", 1, 0, "SocketTester");
    }
};

QUICK_TEST_MAIN_WITH_SETUP(qml_module, Setup)

#include "tst_qml_module.moc"
