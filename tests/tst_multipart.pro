TARGET = tst_multipart

include(authentication_tests.pri)

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${AUTHENTICATION_SRC}/multipart.cpp \
    tst_multipart.cpp

HEADERS += \
    $${AUTHENTICATION_SRC}/multipart.h

DEFINES += \
    TEST_FILE=\\\"$${PWD}/data/story.txt\\\"
