include(tests.pri)

QT += \
    qml

DEFINES += \
    BUILDING_AUTHENTICATION

LIBS += -lsignon-plugins
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/SignOn$${BUILDD}

AUTHENTICATION_SRC = $${TOP_SRC_DIR}/lib/Authentication
