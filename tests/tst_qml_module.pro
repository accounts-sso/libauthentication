TARGET = tst_qml_module

include("../common-config.pri")

CONFIG += no_testcase_installs
CONFIG += qmltestcase

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}
IMPORTPATH += $${TOP_BUILD_DIR}/lib

SOURCES += \
    tst_qml_module.cpp

check.commands = "env LD_LIBRARY_PATH=$${TOP_BUILD_DIR}/lib/Authentication:$${TOP_BUILD_DIR}/lib/SignOn:\$LD_LIBRARY_PATH"
