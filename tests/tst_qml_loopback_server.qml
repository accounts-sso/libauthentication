import Authentication 0.2
import QtQuick 2.3
import QtTest 1.0
import TestSupport 1.0

TestCase {
    id: root

    name: "LoopbackServer"

    Component {
        id: serverComponent
        LoopbackServer {}
    }

    SocketTester {
        id: socketTester
    }

    SignalSpy {
        id: spy
    }

    function checkIsPortOpen(port) {
        return socketTester.connectToPort(port);
    }

    function test_port() {
        var server = serverComponent.createObject(root, {
            listening: false,
        });

        // test default value
        compare(server.port, 0, "Default port is 0");

        server.port = 1231;
        compare(server.port, 1231);
        server.destroy();
    }

    function test_listening() {
        var server = serverComponent.createObject(root, {
            port: 18234,
        });
        compare(server.listening, true);

        verify(checkIsPortOpen(18234));
        server.listening = false;
        tryCompare(server, "listening", false);

        verify(!checkIsPortOpen(18234));
        server.destroy();
    }

    function test_portChangeWhileListening() {
        ignoreWarning("Cannot change port while already listening");
        var server = serverComponent.createObject(root, {
            port: 18234,
        });
        compare(server.listening, true);

        server.port = 13124;
        compare(server.port, 18234);
        server.destroy();
    }

    function test_occupiedPort() {
        var desiredPort = 34125;

        var occupant1 = socketTester.occupyPort(desiredPort);
        var occupant2 = socketTester.occupyPort(desiredPort + 1);

        var server = serverComponent.createObject(root, {
            listening: false,
            port: desiredPort,
            portIncrementAttempts: 3,
        });

        spy.target = server;
        spy.signalName = "portChanged";

        server.listening = true;

        spy.wait();
        compare(spy.count, 1);

        compare(server.port, desiredPort + 2);
        compare(server.listening, true);

        occupant1.destroy();
        occupant2.destroy();

        verify(checkIsPortOpen(desiredPort + 2));
        server.destroy();
    }
}
