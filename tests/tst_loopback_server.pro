TARGET = tst_loopback_server

include(authentication_tests.pri)

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${AUTHENTICATION_SRC}/loopback_server.cpp \
    tst_loopback_server.cpp

HEADERS += \
    $${AUTHENTICATION_SRC}/loopback_server.h
