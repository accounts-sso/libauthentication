/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/HttpServer"
#include <QDebug>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTcpSocket>
#include <QTest>

using namespace Authentication;

class HttpServerTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testCallback();
    void testVisited_data();
    void testVisited();
};

void HttpServerTest::testCallback()
{
    HttpServer server;

    QSignalSpy callbackUrlChanged(&server, SIGNAL(callbackUrlChanged()));

    QVERIFY(callbackUrlChanged.wait());

    QVERIFY(server.callbackUrl().isValid());
    QCOMPARE(server.callbackUrl().host(), QString("localhost"));
    QVERIFY(server.port() != -1);
}

void HttpServerTest::testVisited_data()
{
    QTest::addColumn<QString>("receivedData");
    QTest::addColumn<QUrl>("expectedUrl");

    QTest::newRow("empty") <<
        "" <<
        QUrl();

    QTest::newRow("GET") <<
        "GET /?code=abc&state=212421\r\n"
        "Some other data\r\n" <<
        QUrl("http://localhost:8080/?code=abc&state=212421");
}

void HttpServerTest::testVisited()
{
    QFETCH(QString, receivedData);
    QFETCH(QUrl, expectedUrl);

    HttpServer server;
    QSignalSpy callbackUrlChanged(&server, SIGNAL(callbackUrlChanged()));
    QSignalSpy visited(&server, SIGNAL(visited(const QUrl&)));
    server.setPort(8080);

    QVERIFY(callbackUrlChanged.wait());

    QTcpSocket socket;
    socket.connectToHost("localhost", 8080);
    QVERIFY(socket.waitForConnected());

    socket.write(receivedData.toUtf8());

    if (expectedUrl.isValid()) {
        QTRY_COMPARE(visited.count(), 1);
        QCOMPARE(visited.at(0).at(0).toUrl(), expectedUrl);
    } else {
        QTest::qWait(100);
        QCOMPARE(visited.count(), 0);
    }
}

QTEST_GUILESS_MAIN(HttpServerTest)

#include "tst_http_server.moc"
