/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/AbstractAuthenticator"
#include "Authentication/abstract_authenticator_p.h"
#include <QDebug>
#include <QHash>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <sys/types.h>
#ifndef Q_OS_WIN
#include <unistd.h>
#endif
#include "test_utils.h"

using namespace Authentication;

class DummyAuthenticator: public AbstractAuthenticator
{
    Q_OBJECT

    class DummyAuthenticatorPrivate: public AbstractAuthenticatorPrivate
    {
        friend class DummyAuthenticator;
    public:
        DummyAuthenticatorPrivate(DummyAuthenticator *q_ptr):
            AbstractAuthenticatorPrivate(q_ptr)
        {
        }
        bool buildRequest(AuthenticatedRequest &request,
                          const QUrl &url,
                          const QByteArray &verb,
                          const QVariantMap &params) const override
        {
            return parseParameters(request, url, verb, params);
        }
    };

public:
    DummyAuthenticator(QObject *parent = 0):
        AbstractAuthenticator(new DummyAuthenticatorPrivate(this), parent)
    {
    }

    void process() { Q_EMIT finished(); }

    void storeError(ErrorCode code, const QString &message) {
        setError(code, message);
    }

    void setAccountId(const QString &id);
    void setStoreFilePath(const QString &path);
    void storeData(const QVariantMap &data);
    QVariantMap storedData() const;

private:
    Q_DECLARE_PRIVATE(DummyAuthenticator)
};

void DummyAuthenticator::setAccountId(const QString &id)
{
    AbstractAuthenticator::setAccountId(id);
}

void DummyAuthenticator::setStoreFilePath(const QString &path)
{
    Q_D(DummyAuthenticator);
    d->m_storeFile = path;
}

void DummyAuthenticator::storeData(const QVariantMap &data)
{
    Q_D(DummyAuthenticator);
    d->storeData(data);
}

QVariantMap DummyAuthenticator::storedData() const
{
    Q_D(const DummyAuthenticator);
    return d->storedData();
}

class AbstractAuthenticatorTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testNetworkAccessManager();
    void testError_data();
    void testError();
    void testUserAgent_data();
    void testUserAgent();
    void testHeaders_data();
    void testHeaders();
    void testStore();
    void testLogout();
};

void AbstractAuthenticatorTest::testNetworkAccessManager()
{
    QScopedPointer<QNetworkAccessManager> nam(new QNetworkAccessManager);
    DummyAuthenticator dummy;

    QNetworkAccessManager *ownNam = dummy.networkAccessManager();
    QVERIFY(ownNam);

    QSignalSpy destroyed(ownNam, SIGNAL(destroyed()));

    dummy.setNetworkAccessManager(nam.data());
    QCOMPARE(dummy.networkAccessManager(), nam.data());

    QTRY_COMPARE(destroyed.count(), 1);
}

void AbstractAuthenticatorTest::testError_data()
{
    QTest::addColumn<int>("errorCode");
    QTest::addColumn<QString>("errorMessage");
    QTest::addColumn<bool>("hasError");

    QTest::newRow("no error") <<
        int(AbstractAuthenticator::NoError) << QString() << false;

    QTest::newRow("user canceled") <<
        int(AbstractAuthenticator::UserCanceled) << "your fault" << true;
}

void AbstractAuthenticatorTest::testError()
{
    QFETCH(int, errorCode);
    QFETCH(QString, errorMessage);
    QFETCH(bool, hasError);

    QScopedPointer<QNetworkAccessManager> nam(new QNetworkAccessManager);
    DummyAuthenticator dummy(nam.data());

    dummy.storeError(AbstractAuthenticator::ErrorCode(errorCode), errorMessage);

    QCOMPARE(dummy.hasError(), hasError);
    QCOMPARE(dummy.errorCode(), AbstractAuthenticator::ErrorCode(errorCode));
    QCOMPARE(dummy.errorMessage(), errorMessage);
}

void AbstractAuthenticatorTest::testUserAgent_data()
{
    QTest::addColumn<QString>("userAgent");
    QTest::addColumn<Headers>("expectedHeaders");

    QTest::newRow("no user-agent") <<
        QString() << Headers();

    QTest::newRow("Mozilla") <<
        "Mozilla/5.0 (X11; Linux i686; rv:10.0) Gecko/20100101" <<
        Headers {
            { "User-Agent", "Mozilla/5.0 (X11; Linux i686; rv:10.0) Gecko/20100101" },
        };
}

void AbstractAuthenticatorTest::testUserAgent()
{
    QFETCH(QString, userAgent);
    QFETCH(Headers, expectedHeaders);

    DummyAuthenticator dummy;

    dummy.setUserAgent(userAgent);
    QCOMPARE(dummy.userAgent(), userAgent);

    QScopedPointer<QNetworkReply> reply(dummy.get(QUrl("http://example.com"),
                                                  QVariantMap()));
    QVERIFY(!reply.isNull());

    QNetworkRequest req = reply->request();
    QCOMPARE(requestHeaders(req), expectedHeaders);
}

void AbstractAuthenticatorTest::testHeaders_data()
{
    QTest::addColumn<QVariantMap>("params");
    QTest::addColumn<Headers>("expectedHeaders");

    QTest::newRow("empty") <<
        QVariantMap() << Headers();

    QTest::newRow("some params") <<
        QVariantMap {
            { "Verbose", "yes" },
            { "Content-Type", "This is not a header :-)" },
        } <<
        Headers();

    QTest::newRow("accept") <<
        QVariantMap {
            { "headers",
                QVariantMap {
                    { "Accept", "text/html" },
                }
            },
        } <<
        Headers {
            { "Accept", "text/html" },
        };
}

void AbstractAuthenticatorTest::testHeaders()
{
    QFETCH(QVariantMap, params);
    QFETCH(Headers, expectedHeaders);

    DummyAuthenticator dummy;

    QScopedPointer<QNetworkReply> reply(dummy.get(QUrl("http://example.com"),
                                                  params));
    QVERIFY(!reply.isNull());

    QNetworkRequest req = reply->request();
    QCOMPARE(requestHeaders(req), expectedHeaders);
}

void AbstractAuthenticatorTest::testStore()
{
    DummyAuthenticator dummy;

    QVariantMap data1 {
        { "key", "value" },
        { "boolean", true },
    };

    dummy.setAccountId("1");
    QCOMPARE(dummy.property("accountId").toString(), QString("1"));
    dummy.storeData(data1);
    QCOMPARE(dummy.storedData(), data1);

    /* now store under a different account ID */
    QVariantMap data2 {
        { "int", int(43) },
        { "boolean", false },
    };

    dummy.setAccountId("2");
    dummy.storeData(data2);
    QCOMPARE(dummy.storedData(), data2);

    /* Data1 must still be there! */
    dummy.setAccountId("1");
    QCOMPARE(dummy.storedData(), data1);

    /* Test error paths, with non creatable dirs */
#ifndef Q_OS_WIN
    if (getuid() != 0) {
        QTest::ignoreMessage(QtWarningMsg, "Could not create store file \"/file.json\"");
        QTest::ignoreMessage(QtWarningMsg, "Could not create store dir!");
        QVariantMap data3 {
            { "int", int(12) },
            { "string", "indeed" },
        };

        dummy.setStoreFilePath("/file.json");
        dummy.storeData(data3);

        dummy.setStoreFilePath("/dir/file.json");
        dummy.storeData(data3);
    }
#endif
}

void AbstractAuthenticatorTest::testLogout()
{
    DummyAuthenticator dummy;

    QVariantMap data1 {
        { "key", "value" },
        { "boolean", true },
    };

    dummy.setAccountId("1");
    dummy.storeData(data1);
    QCOMPARE(dummy.storedData(), data1);

    /* now store under a different account ID */
    QVariantMap data2 {
        { "int", int(43) },
        { "boolean", false },
    };

    dummy.setAccountId("2");
    dummy.storeData(data2);
    QCOMPARE(dummy.storedData(), data2);

    dummy.logout();

    /* Data1 must still be there! */
    dummy.setAccountId("1");
    QCOMPARE(dummy.storedData(), data1);

    /* ...while data2 should be gone */
    dummy.setAccountId("2");
    QCOMPARE(dummy.storedData(), QVariantMap());
}

QTEST_GUILESS_MAIN(AbstractAuthenticatorTest)

#include "tst_abstract_authenticator.moc"
