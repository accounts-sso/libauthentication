/*
 * This file is part of libAuthentication
 *
 * Copyright (C) 2017-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "Authentication/LoopbackServer"
#include <QDebug>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTest>

using namespace Authentication;

class LoopbackServerTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testProperties();
    void testVirtualDestructor();
    void testCallback();
    void testPartialRead();
    void testVisited_data();
    void testVisited();
    void testPortIncrement_data();
    void testPortIncrement();
    void testClose();
    void testServedHtml();
};

void LoopbackServerTest::testProperties()
{
    LoopbackServer server;

    /* Test default values */
    QCOMPARE(server.portIncrementAttempts(), 0);
    QCOMPARE(server.servedHtml(), QByteArrayLiteral(
        "<html><head><title>tst_loopback_server</title></head><body>"
        "Authentication completed; you can close this page."
        "</body></html>"));
    QVERIFY(server.callbackUrl().isEmpty());

    /* Test the setters */
    server.setPortIncrementAttempts(4);
    QCOMPARE(server.portIncrementAttempts(), 4);

    QByteArray newHtml = QByteArrayLiteral("<body></body>");
    server.setServedHtml(newHtml);
    QCOMPARE(server.servedHtml(), newHtml);
}

void LoopbackServerTest::testVirtualDestructor()
{
    /* Just to cover all functions */
    LoopbackServer *server = new LoopbackServer();
    delete server;
}

void LoopbackServerTest::testCallback()
{
    LoopbackServer server;
    QSignalSpy callbackUrlChanged(&server, SIGNAL(callbackUrlChanged()));

    QVERIFY(server.listen());
    QCOMPARE(callbackUrlChanged.count(), 1);
    QVERIFY(server.isListening());

    QVERIFY(server.callbackUrl().isValid());
    QCOMPARE(server.callbackUrl().host(), QString("localhost"));
    QVERIFY(server.port() != 0);
}

void LoopbackServerTest::testPartialRead()
{
    LoopbackServer server;
    QSignalSpy visited(&server, SIGNAL(visited(const QUrl&)));
    QVERIFY(server.listen());

    QTcpSocket socket;
    socket.connectToHost("localhost", server.port());
    QVERIFY(socket.waitForConnected());

    socket.write("GET ");
    socket.flush();

    /* make sure that the server is still waiting */
    QTest::qWait(20);
    QVERIFY(socket.read(20).isEmpty());

    /* Send the rest of the data; we should then get a reply */
    socket.write("/something\r\n");

    QTRY_COMPARE(visited.count(), 1);
    QUrl expectedUrl(server.callbackUrl());
    expectedUrl.setPath("/something");
    QCOMPARE(visited.at(0).at(0).toUrl(), expectedUrl);
}

void LoopbackServerTest::testVisited_data()
{
    QTest::addColumn<QString>("receivedData");
    QTest::addColumn<QUrl>("expectedUrl");

    QTest::newRow("empty") <<
        "" <<
        QUrl();

    QTest::newRow("GET") <<
        "GET /?code=abc&state=212421\r\n"
        "Some other data\r\n" <<
        QUrl("http://localhost:51244/?code=abc&state=212421");
}

void LoopbackServerTest::testVisited()
{
    QFETCH(QString, receivedData);
    QFETCH(QUrl, expectedUrl);

    LoopbackServer server;
    QSignalSpy callbackUrlChanged(&server, SIGNAL(callbackUrlChanged()));
    QSignalSpy visited(&server, SIGNAL(visited(const QUrl&)));

    QVERIFY(server.listen(51244));

    QTcpSocket socket;
    socket.connectToHost("localhost", server.port());
    QVERIFY(socket.waitForConnected());

    socket.write(receivedData.toUtf8());

    if (expectedUrl.isValid()) {
        QTRY_COMPARE(visited.count(), 1);
        QCOMPARE(visited.at(0).at(0).toUrl(), expectedUrl);
    } else {
        QTest::qWait(100);
        QCOMPARE(visited.count(), 0);
    }
}

void LoopbackServerTest::testPortIncrement_data()
{
    QTest::addColumn<int>("usedPorts");
    QTest::addColumn<int>("maxAttempts");
    QTest::addColumn<bool>("expectedSuccess");

    QTest::newRow("no retry attempts") <<
        2 <<
        0 <<
        false;

    QTest::newRow("not enough retry attempts") <<
        2 <<
        1 <<
        false;

    QTest::newRow("enough retry attempts") <<
        3 <<
        3 <<
        true;
}

void LoopbackServerTest::testPortIncrement()
{
    QFETCH(int, usedPorts);
    QFETCH(int, maxAttempts);
    QFETCH(bool, expectedSuccess);

    const uint16_t port = 12463;

    /* Occupy the first `usedPorts` ports */
    const QVector<QTcpServer> occupants(usedPorts);
    for (uint16_t i = 0; i < usedPorts; i++) {
        QTcpServer &occupant = const_cast<QTcpServer&>(occupants[i]);
        QVERIFY(occupant.listen(QHostAddress::Any, port + i));
    }

    /* Start our server */
    LoopbackServer server;
    server.setPortIncrementAttempts(uint16_t(maxAttempts));

    bool success = server.listen(port);
    QCOMPARE(success, expectedSuccess);
}

void LoopbackServerTest::testClose()
{
    LoopbackServer server;

    QVERIFY(server.listen());

    {
        QTcpSocket socket;
        socket.connectToHost("localhost", server.port());
        QVERIFY(socket.waitForConnected());
    }

    /* Now close the server; we shouldn't be able to connect anymore */
    server.close();

    QTcpSocket socket;
    socket.connectToHost("localhost", server.port());
    QVERIFY(!socket.waitForConnected());
}

void LoopbackServerTest::testServedHtml()
{
    LoopbackServer server;
    QVERIFY(server.listen());

    QByteArray html =
        QByteArrayLiteral("<body>Hello, world!</body>");
    server.setServedHtml(html);

    QTcpSocket socket;
    socket.connectToHost("localhost", server.port());
    QVERIFY(socket.waitForConnected());
    QSignalSpy disconnected(&socket, SIGNAL(disconnected()));

    socket.write("whatever\r\n");
    QTRY_COMPARE(disconnected.count(), 1);

    QByteArray reply = socket.readAll();
    int doubleEmptyLine = reply.indexOf("\r\n\r\n");
    QVERIFY(doubleEmptyLine >= 0);

    QByteArray content = reply.mid(doubleEmptyLine + 4);
    QCOMPARE(content, html);
}

QTEST_GUILESS_MAIN(LoopbackServerTest)

#include "tst_loopback_server.moc"
