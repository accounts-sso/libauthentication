include(../../common-config.pri)
TARGET=testplugin
TEMPLATE = lib

CONFIG += \
    plugin

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib
LIBS += -lsignon-plugins
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/SignOn$${BUILDD}

SOURCES += \
    test_plugin.cpp

HEADERS += \
    test_plugin.h
