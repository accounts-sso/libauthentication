/*
 * This file is part of signon
 *
 * Copyright (C) 2017 mardy.it
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "test_plugin.h"

using namespace TestPlugin;

namespace TestPlugin {
SIGNON_DECL_AUTH_PLUGIN(Plugin)
} // namespace

Plugin::Plugin(QObject *parent):
    AuthPluginInterface(parent)
{
}

Plugin::~Plugin()
{
}

QString Plugin::type() const
{
    return "test-plugin";
}

QStringList Plugin::mechanisms() const
{
    return QStringList { "password", "other" };
}

void Plugin::cancel()
{
}

void Plugin::process(const SignOn::SessionData &inData,
                     const QString &mechanism)
{
	Q_UNUSED(mechanism);
    Q_EMIT result(QVariantMap(inData.toMap()));
}

void Plugin::userActionFinished(const SignOn::UiSessionData &data)
{
    Q_EMIT result(QVariantMap(data.toMap()));
}

void Plugin::refresh(const SignOn::UiSessionData &data)
{
	Q_UNUSED(data);
    Q_EMIT error(SignOn::Error(SignOn::Error::TimedOut, "Some error"));
}

AuthPluginInterface *PluginInterface::createAuthPlugin(QObject *parent)
{
    return new Plugin(parent);
}
