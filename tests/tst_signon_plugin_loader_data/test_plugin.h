/*
 * This file is part of signon
 *
 * Copyright (C) 2017 mardy.it
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <SignOn/AuthPluginInterface>

#include <QObject>

namespace TestPlugin {

class Plugin: public AuthPluginInterface
{
    Q_OBJECT
    Q_INTERFACES(AuthPluginInterface)

public:
public:
    Plugin(QObject *parent = 0);
    ~Plugin();

public Q_SLOTS:
    QString type() const override;
    QStringList mechanisms() const override;
    void cancel() override;
    void process(const SignOn::SessionData &inData, const QString &mechanism = 0) override;
    void userActionFinished(const SignOn::UiSessionData &data) override;
    void refresh(const SignOn::UiSessionData &data) override;
};

class PluginInterface: public QObject, AuthPluginInterface2
{
    Q_OBJECT
    Q_INTERFACES(AuthPluginInterface2)
    Q_PLUGIN_METADATA(IID "com.nokia.SingleSignOn.PluginInterface/2")

public:
    AuthPluginInterface *createAuthPlugin(QObject *parent = 0) override;
};

} // namespace
