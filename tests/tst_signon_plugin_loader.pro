TARGET = tst_signon_plugin_loader

include(tests.pri)

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib
LIBS += -lsignon-plugins
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/SignOn$${BUILDD}
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    tst_signon_plugin_loader.cpp

DEFINES += \
    PLUGIN_DIR=\\\"$${OUT_PWD}/tst_signon_plugin_loader_data\\\"
