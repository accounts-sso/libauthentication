TEMPLATE = subdirs
SUBDIRS = \
    tst_abstract_authenticator.pro \
    tst_http_server.pro \
    tst_loopback_server.pro \
    tst_multipart.pro \
    tst_oauth1.pro \
    tst_oauth2.pro \
    tst_plugin_loader.pro \
    tst_plugin_loader_subdirs.pro \
    tst_qml_module.pro
