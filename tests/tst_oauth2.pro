TARGET = tst_oauth2

include(authentication_tests.pri)

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib/SignOn \
    $${TOP_SRC_DIR}/plugins/signon-oauth2/src

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${AUTHENTICATION_SRC}/abstract_authenticator.cpp \
    $${AUTHENTICATION_SRC}/action_request.cpp \
    $${AUTHENTICATION_SRC}/multipart.cpp \
    $${AUTHENTICATION_SRC}/oauth2.cpp \
    tst_oauth2.cpp

HEADERS += \
    $${AUTHENTICATION_SRC}/abstract_authenticator.h \
    $${AUTHENTICATION_SRC}/abstract_authenticator_p.h \
    $${AUTHENTICATION_SRC}/action_request.h \
    $${AUTHENTICATION_SRC}/multipart.h \
    $${AUTHENTICATION_SRC}/oauth2.h \
    fake_network.h
