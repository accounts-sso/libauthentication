TARGET = tst_http_server

include(authentication_tests.pri)

QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${AUTHENTICATION_SRC}/http_server.cpp \
    tst_http_server.cpp

HEADERS += \
    $${AUTHENTICATION_SRC}/http_server.h
